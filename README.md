### Disclaimer


Никакой материал отсюда не претендует на звание полноценной публичной статьи или руководства. Это мои **личные заметки**.
Если кому-то статьи полезны, то хорошо. Если нет, то нет.
<br>
Я исправляю ошибки в статьях и дополняю когда есть желание. Я знаю об ошибка в статьях, я не слежу за особой точностью статей. В первую очередь статьи понятны мне. Исправляю статьи только по мере **моей** надобности.


### Статьи


Статьи:
* [Firefox](src/firefox/desktop.md)
* [Thunderbird](src/thunderbird/desktop.md)
* [Firefox для Android](src/firefox/android.md)
* [Android](src/android/README.md)
* [Windows](src/win/README.md)


Статьи о Ubuntu:
* [Как включить Hibernation (гибернацию, спящий режим)](src/linux/hibernation.md)
* [Как настроить программу DNSCRYPT (шифрованные DNS запросы для всей системы, чтобы избежать блокировок сайтов по DNS, рекламы и цензуры со стороны провайдера)](src/linux/dnscrypt_setup.md)
* [Делаем полное шифрование Ubuntu с загрузчиком хранимым на USB Flash](src/linux/full_disk_encryption_&_efi_on_usb_flash.md) - **Коротко** о том что это: **Windows** (незашированная) + **Ubuntu** (зашированная) + **GRUB** на Flash Drive (незаширован, но можно и нужно вынимать при загрузке в Windows)
* [BFQ scheduler enabling](src/linux/bfq_enabling.md)
* [KSM enabling](src/linux/ksm_enabling.md)
* [Настройка OOM Killer](src/linux/solve_oom.md) - программы убивающий процессы при нехватке память, чтобы система не зависала
* [Настройка Ubuntu после установки системы](src/linux/ubuntu_after_installation.md)

Другое:
* [Обход ограничения операторами на раздачу wi-fi](src/other/bypass_gsm_operator_wifi_hotspot_restriction_on_PC.md)
* [Список онлайн карт](src/other/0_maps.md)
* [Как выбрать телефон](src/other/how_to_choose_phone.md)
