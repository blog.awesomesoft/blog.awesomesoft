Краткий обзор телефонов, какой выбрать и плюсы с минусами [тут](src/other/how_to_choose_phone.md)

**Note.** Здесь только бесплатные программы. В том числе могут содержать рекламу или отправку статистики, о чем указано отдельно в описании каждой программы.

**Предупреждение.** Если программа с открытым кодом все равно зайдите в ее настройки и проверьте отключена ли отправка статистики.

Если по ссылке программа не доступна (ссылка сломана), то ищите программу сами. <sub>особенно в **F-Droid** любят менять ссылки</sub>

- [Программы с открытым кодом](#программы-с-открытым-кодом)
  - [Источники программ (магазины, репозиторий)](#источники-программ-магазины-репозиторий)
  - [Медиа](#медиа)
  - [Камера](#камера)
  - [Навигация](#навигация)
  - [Встроенные и подключаемые устройства](#встроенные-и-подключаемые-устройства)
  - [Интернет](#интернет)
  - [Органайзеры и справочники](#органайзеры-и-справочники)
  - [Файлы](#файлы)
  - [Оффис](#оффис)
  - [Приватность](#приватность)
  - [Игры](#игры)
  - [Система](#система)
  - [Профессиональные](#профессиональные)
    - [Астрономия](#астрономия)
    - [Математика](#математика)
    - [Физика](#физика)
    - [Черчение](#черчение)
  - [Программирование, системное администрирование](#программирование-системное-администрирование)
  - [Шифрование и изоляции (программ и системы)](#шифрование-и-изоляции-программ-и-системы)
- [Программы с открытым кодом для root](#программы-с-открытым-кодом-для-root)
  - [Интернет](#интернет-1)
- [Программы с открытым кодом для ADB, Shizuku, Dhizuku](#программы-с-открытым-кодом-для-adb-shizuku-dhizuku)
- [Программы с закрытым кодом](#программы-с-закрытым-кодом)
  - [Предупреждение](#предупреждение)
  - [Источники программ (магазины, репозиторий)](#источники-программ-магазины-репозиторий-1)
  - [Медиа](#медиа-1)
  - [Камера](#камера-1)
  - [Навигация](#навигация-1)
  - [Встроенные и подключаемые устройства](#встроенные-и-подключаемые-устройства-1)
  - [Интернет](#интернет-2)
  - [Органайзеры и справочники](#органайзеры-и-справочники-1)
  - [Файлы](#файлы-1)
  - [Оффис](#оффис-1)
  - [Услуги](#услуги)
  - [Система](#система-1)
  - [Программирование, системное администрирование](#программирование-системное-администрирование-1)
  - [Шифрование и изоляции (программ и системы)](#шифрование-и-изоляции-программ-и-системы-1)
- [Советы, заметки и решение проблем](#советы-заметки-и-решение-проблем)
  - [Чего не хватает в этом списке](#чего-не-хватает-в-этом-списке)
  - [Почему ссылки на **F-Droid** в последнюю очередь?](#почему-ссылки-на-f-droid-в-последнюю-очередь)
  - [Почему может не работать авто синхронизация в некоторых программах (например уведомления о почте в **K9 Mail**)?](#почему-может-не-работать-авто-синхронизация-в-некоторых-программах-например-уведомления-о-почте-в-k9-mail)
  - [Почему могут не появляться уведомления (notifications) от **K9 Mail** и других программ? Почему некоторые программ работают с задержкой или не правильно работают в фоне (на срабатываю или завершаются)? Что делать? (Это из-за оптимизатора батареи, он их тормозит и завершает)](#почему-могут-не-появляться-уведомления-notifications-от-k9-mail-и-других-программ-почему-некоторые-программ-работают-с-задержкой-или-не-правильно-работают-в-фоне-на-срабатываю-или-завершаются-что-делать-это-из-за-оптимизатора-батареи-он-их-тормозит-и-завершает)
  - [Почему могут не работать сторонние будильники и таймеры?](#почему-могут-не-работать-сторонние-будильники-и-таймеры)
  - [Почему может не работать вход в почту (например Gmail) через **K9 Mail** и другие?](#почему-может-не-работать-вход-в-почту-например-gmail-через-k9-mail-и-другие)
- [Слишком громкий или тихий звук при звонке в Android](#слишком-громкий-или-тихий-звук-при-звонке-в-android)
  - [MicroG. Почему без Google сервисов не работает навигация и другие функции в некоторых программах?](#microg-почему-без-google-сервисов-не-работает-навигация-и-другие-функции-в-некоторых-программах)
  - [Стандартные приложения Android от Google (в том числе если производитель телефона заменил их своими)](#стандартные-приложения-android-от-google-в-том-числе-если-производитель-телефона-заменил-их-своими)
  - [Про программы камеры для фото и видео и о том что же выбрать](#про-программы-камеры-для-фото-и-видео-и-о-том-что-же-выбрать)
  - [Скрытие приложений, фото и прочего в Android](#скрытие-приложений-фото-и-прочего-в-android)

# Программы с открытым кодом

## Источники программ (магазины, репозиторий)
* [F-Droid](https://f-droid.org/) - репозиторий программ с открытым кодом (часто версии программ в нём устаревшие, зато проверенные). В настройках программы можно включить дополнительный источник программ от [Guardian Project](https://guardianproject.info/) (Tor, приватные мессенджеры и так далее).
	* **Предупреждение:** [F-Droid](https://f-droid.org) перекомпилирует приложения, бывает с ошибками. Если программа не работает - пробуйте установить её из других источников, оригинально скомпилированную. Некоторые open source программы могут туда не попасть - у владельцев [F-Droid](https://f-droid.org) просто не хватает знаний на компилирование. Если open source программа использует какую-то несвободную библиотеку, то она тоже может туда не попасть, из-за политики [F-Droid](https://f-droid.org) использовать только open source программы.
* [Aptoide](https://www.aptoide.com/) - формально код открыт, но на деле приложения хранятся онлайн на сайте aptoide.com, каждый пользователь может создать свой репозиторий. В результате - много поддельных вредоносных приложений, что-то стоящее найти очень трудно. **Не рекомендуется**, если вы не хотите сделать свой собственный репозиторий.
* [Yalp Store](https://f-droid.org/repository/browse/?fdfilter=store&fdid=com.github.yeriomin.yalpstore) - **перестал разрабатываться хотя по прежнему может работать**, программа может напрямую скачивать `.apk` из **Google Play**, обновлять и т.д.
* [Aurora Store](http://auroraoss.com/) - аналог [Yalp Store](https://f-droid.org/repository/browse/?fdfilter=store&fdid=com.github.yeriomin.yalpstore) (часть кода из него), но со своими фичами и активно разрабатывается.

## Медиа
* [VLC](http://download.videolan.org/pub/videolan/vlc-android/) - универсальный плеер для аудио и видео, играет почти все существующие форматы. В нем иногда встречаются баги, и CPU может нагружать.
* [Odyssey Music Player](https://github.com/gateship-one/odyssey) - музыкальный плеер требующий мало ресурсов.
* [Audio Recorder](https://play.google.com/store/apps/details?id=com.github.axet.audiorecorder) - диктофон, пишет в форматах .ogg, .wav, .flac, .m4a, .mka, .mp3. Есть настройка качества записи, паузы во время разговора по телефону, выбор каталога.
* [Call recorder for Android](https://github.com/riul88/call-recorder-for-android) - автоматическая запись телефонных разговоров

## Камера
* [Open Camera](https://sourceforge.net/projects/opencamera/files/) - альтернативная камера для видео и фото с множеством настроек и функций, автоопределением лиц
* [CosyDVR](http://cosydvr.esy.es/doku.php) - делает из камеры телефона видеорегистратор, пишет видео в фоне. Версия на [F-Droid](https://f-droid.org/repository/browse/?fdfilter=CosyDVR&fdid=es.esy.CosyDVR), версия для старых Android на [F-Droid](https://f-droid.org/repository/browse/?fdid=com.example.CosyDVR)
* [Camera Adversaria](https://play.google.com/store/apps/details?id=com.kieranbrowne.cameraadversaria) -  добавляет невидимый шум на фото который не дает системам распознавания лиц и прочих объектов в соц. сетях распознать их и использовать для рекламы или продажи анализа данных. На системы распознавания спецслужб, модели которых строятся на основе достоверных источников, опубликованный инструментарий повлияет в меньшей степени. **Note.** в документации указано, что лицензия GPL, но нету ссылки на код и страница [найденная страница с кодом](https://github.com/kieranbrowne/camera-adversaria) не содержит лицензии в коде. Справку по разным алгоритмам и проектам сокрытия лиц на фотографиях от распознавания можно посмотреть [тут](https://www.opennet.ru/opennews/art.shtml?num=53414)

## Навигация
* [OsmAnd~](https://f-droid.org/packages/net.osmand.plus/) (OsmAnd+) - показывает карты OpenStreetMap без интернета (не путать с версией из Google Play в которой есть **ограничение** на количество используемых карт, версия F-Droid пересобрана **без этих ограничений**). <sub>Вы должны понимать, что не смотря на все плюсы данные карты **не заменят** ```Google Maps``` или ```Yandex Maps```, которые сделаны более профессионально</sub>
* [GPSTest](https://play.google.com/store/apps/details?id=com.android.gpstest) или [SatStat](https://f-droid.org/repository/browse/?fdfilter=satstat&fdid=com.vonglasow.michael.satstat) - показывает найденные спутники, полезно чтобы знать в какой местности какая точность позиционирования
* [Trail Sense](https://play.google.com/store/apps/details?id=com.kylecorry.trail_sense) - набор утилит для навигации в незнакомой местности, GPS, пройденный маршрут с возможностью установки маяков, высотомер (при наличии барометра и по картам GPS), фонарь с режимом SOS, компас, положение солнца и луны, время восхода и заката, линейка, измерение высоты скал по времени спуска, расчет расстояния до молнии, конвертер величин расстояния, расчет положение по триангуляции без GPS, строительный уровень, расчет время закипания воды, расчет оптимального положения солнечных панелей, детектор металла (на основе датчика магнитометра компаса телефона), термометр телефона, справочник типов облаков с картинками, заметки, точная информация о заряде батареи, конвертер координат, свисток etc.
* [Look4Sat: Radio satellite tracker](https://play.google.com/store/apps/details?id=com.rtbishop.look4sat) - трекер навигационных спутников
* [Magic Earth Navigation & Maps](https://play.google.com/store/apps/details?id=com.generalmagic.magicearth) - программа навигации по картам OpenStreetMap со своими фичами

## Встроенные и подключаемые устройства

- **IR Remote Control** - инфра красный порт для управления устройствами как пультом
  - [Mi Remote controller](https://play.google.com/store/apps/details?id=com.duokan.phone.remotecontroller)

## Интернет
* **Менеджеры загрузок, ftp клиенты, торрент клиенты**
  * [LibreTorrent](https://f-droid.org/repository/browse/?fdfilter=torrent&fdid=org.proninyaroslav.libretorrent) или [Torrent Client](https://f-droid.org/packages/com.github.axet.torrentclient/) - торрент клиент
  * [Download Navi - Download Manager](https://github.com/TachibanaGeneralLaboratories/download-navi) или [GigaGet Download Manager](https://f-droid.org/repository/browse/?fdfilter=GigaGet&fdid=us.shandian.giga) - менеджер загрузок, может качать в несколько потоков, можно выбрать каталог сохранения и прочее. **Note:** _GigaGet Download Manager_ таинственно исчезла со всех ресурсов.
  * [uGet](https://play.google.com/store/apps/details?id=com.ugetdm.uget) - менеджер загрузок
* **Мессенджеры**
  * [Conversations](https://f-droid.org/repository/browse/?fdfilter=conversation&fdid=eu.siacs.conversations) - обмен сообщениями по протоколу XMPP (Jabber), поддерживает стойкое к взлому шифрование OTR на стороне клиента. То есть через него можно абсолютно приватно общаться с другом (для работы нужно зарегистрироваться на любом XMPP сервере)
* **Социальные сети и сервисы**
  * [NewPipe](https://f-droid.org/en/packages/org.schabi.newpipe/) - клиент для youtube, есть старая версия [NewPipe Legacy](https://f-droid.org/en/packages/org.schabi.newpipelegacy/)
  * [Spotube](https://spotube.krtirtho.dev/) - неофициальный беслпатный клиент Spotify, бесплатно доступны платные функции. Парсит плей листы и загружает музыку из других источников.
* **AI bots & services**
  * [​​Microsoft Copilot](https://play.google.com/store/apps/details?id=com.microsoft.copilot)
  * [Gemini](https://gemini.google.com/)
  * [Yandex Alice](https://a.ya.ru)
* **Почтовые клиенты**
  * [K-9 Mail](https://github.com/k9mail/k-9/releases) - почтовый клиент. Для работы с GMail и некоторыми другими сервисами нужно зайти в сервис с браузера и включить возможность доступа программам к почте.
    * **Note.** чтобы работать с gmail нужно в настройках аккаунта google в разделе "Безопасность" (Security) разрешить подключение "небезопасных приложений" (less security application); на самом деле никакой опасности тут нет. (если ссылка не изменилась, то менять [тут](https://myaccount.google.com/lesssecureapps))
    * **Note.** В **K9 Mail** может быть баг с тем что синхронизация (получение уведомлений о приходе писем) не срабатывает когда авто-синхронизация включена в настройках или вы можете хотеть синхронизацию в **K9 Mail** когда она отключена в телефоне. Для вкючения синхронизации зайдите в настройки, пункт **Сеть** (Network) и поставить **Фоновая синхронизация** (Background sync) в значение **всегда** (always)
    * **Note.** Чтобы **гарантированно получать уведомления** о новой почте не забудьте отключить в настройках батареи все оптимизации расхода заряда, отключите и общие и конкретно для **K9 Mail**. В настройках прав (разрешений) разрешите для **K9 Mail** все что можно.
  * [Librem Mail](https://f-droid.org/en/packages/one.librem.mail/) - ответвление от **K9 Mail**, с поддержкой шифрование end-to-end (т.е. сообщение зашифровано и владелец почтового сервера не может его прочитать)
  * [FairEmail](https://play.google.com/store/apps/details?id=eu.faircode.email) - почтовый клиент
* **Браузеры**
  * [Firefox](https://ftp.mozilla.org/pub/mobile/releases/) - браузер, может блокировать рекламу и сохранять/открывать страницы одним файлом через установленные расширения etc. Больше о его настройке по [ссылке](https://github.com/myBestSoftAndPref/soft/blob/master/src/firefox/android.md)
  * [Chromium](https://download-chromium.appspot.com/?platform=Android&type=snapshots) - браузер на основе которого делается Chrome
  * [Tor Browser](https://play.google.com/store/apps/details?id=org.torproject.torbrowser) - анонимный браузер от проекта Tor.
* **Разное**
  * [Google Authenticator](https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2) - необходимо на некоторых сайтах и сервисах использующих Двух-Факторный вход (2-Step Verification) для входа, без него вход может быть невозможен
    * Существует множество альтернатив данному приложению
    * Рекомендую всегда иметь при себе иначе сервисы могут быть недоступны.
    * Аккаунт можно сохранить, так что при повторных входах код будет генерировать на телефоне автоматически, и видимо привязываться к ID операционной системы устройства и ID оборудования устройства (e.g. телефона на котором установлен).
    * Кроме кода для входа сгенерируется несколько кодов для восстановления доступа, их нужно сохранить, без них можно потерять доступ в случае смены телефона.

## Органайзеры и справочники
* [Search Light](https://f-droid.org/repository/browse/?fdfilter=search&fdid=com.scottmain.android.searchlight&fdpage=4) - вспышка в качестве фонарика
* [Torchie](https://f-droid.org/repository/browse/?fdid=in.blogspot.anselmbros.torchie) - **не рекомендуется**, вспышка в качестве фонарика. Очень удобное включение удерживанием кнопок громкости только в режиме когда телефон блокирован и не работает датчик приближения (телефон в кармане). Поиграйтесь с настройками (**ОСТОРОЖНО** может вызвать такие проблемы, что телефоном вы сможете пользоваться только после удаления этой программы)
* [Omni Notes](https://github.com/federicoiosue/Omni-Notes) - развитое приложение для to do заметок разных типов (текст, аудио и прочее)
* [QuickDic restored](https://github.com/rdoeffinger/Dictionary/releases) - словарь иностранных слов, работает без интернета
* [QDict](https://github.com/madmanteam/QDict) - словарь, поддерживает формат ```StarDict``` в котором существует очень много словарей
* [Binary Eye](https://play.google.com/store/apps/details?id=de.markusfisch.android.binaryeye) или [Barcode Scanner](https://github.com/zxing/zxing/releases) - сканер штрих кодов, QR кодов и их создание с поиском информации по ним через интернет браузер
* [Bubble](https://f-droid.org/repository/browse/?fdid=net.androgames.level) - строительный уровень, показывает угол наклона поверхностей на основе датчика гироскоп. Есть [новая версия](https://play.google.com/store/apps/details?id=net.androgames.level) этой программ на **Google Play**, но она содержит рекламу и ее код закрыт.
* [Unit Converter Ultimate](https://f-droid.org/repository/browse/?fdfilter=converter&fdid=com.physphil.android.unitconverterultimate) - конвертер величин (граммы в пинты, килограммы, футы в метры и так далее)
* [Ruler](https://f-droid.org/repository/browse/?fdfilter=ruler&fdid=org.secuso.privacyfriendlyruler) - на дисплее отображается транспортир и линейка
* [SpeedMeter](https://play.google.com/store/apps/details?id=fly.speedmeter.grub) - опеределение скорости движения на основе данных GPS.
* [Forecastie](https://play.google.com/store/apps/details?id=com.casticalabs.forecastie) - просмотр данных о погоде из сети (температура, давление, осадки и прочее), есть карта температур, ветров, дождей и графики.
* [Weather](https://play.google.com/store/apps/details?id=org.secuso.privacyfriendlyweather) - удобный просмотр погоды по городам, но нет карты погоды.
* [PalmCalc 2019+](https://f-droid.org/en/packages/com.github.palmcalc2019.palmcalc/) - это ответвление от популярного калькулятора **PalmCalc**, код оригинальной версии больше не открытый, и оригинальный **PalmCalc** почему-то удален из Google Play. Сайт оригинального PalmCalc: http://palmcalc.com/
* [Lifeograph](https://play.google.com/store/apps/details?id=net.sourceforge.lifeograph) - дневник с зашифрованными записями, есть [версия для настольного компьютера](http://lifeograph.sourceforge.net/wiki/Main_Page)
* [Joplin](https://play.google.com/store/apps/details?id=net.cozic.joplin) - заметки, в том числе в markdown
  * **Note.** Есть версия для Windows и Linux [тут](https://joplinapp.org/desktop/)
* [Suntimes](https://f-droid.org/packages/com.forrestguice.suntimeswidget/) - показывает время восхода и заката для разных часовых поясов и разных типов, состояние луны и другую подобную информацию.
  * [Suntimes Calendars](https://f-droid.org/en/packages/com.forrestguice.suntimescalendars/) - дополнение к **Suntimes**, может добавить в **стандартный** календарь (к датам календаря) время восхода, заката, фазы луны и прочее (может не работать со сторонними календарями)
* [Simple Calendar Pro](https://f-droid.org/en/packages/com.simplemobiletools.calendar.pro/) - простой календарь без привязки к Google Service
  * **Note.** В нем может не работать интеграция с другими программами из-за неиспользования Google Service

## Файлы
* [Ghost Commander](https://sourceforge.net/projects/ghostcommander/files/Releases/) - двух панельный файловый менеджер, есть поддержка zip, WebDav, Box, SFTP, Samba, Google Drive, Dropbox (дополнительные функции устанавливаются через плагины). Плагины в Google Play можно найти по [этой](https://play.google.com/store/apps/developer?id=Ghost+Squared) ссылке, плагины на сайте разработчика по [этой](https://sourceforge.net/projects/ghostcommander/files/Releases/) (в F-Droid ограниченный набор плагинов).

## Оффис
* [Cool Reader](https://sourceforge.net/projects/crengine/files/) - удобная читалка для fb2, epub, chm, rtf, doc, html и других форматов, поддерживает множество форматов и opds онлайн каталоги
* [FBReader](https://fbreader.org/FBReaderJ) - не такая удобная, как ```Cool Reader```, но имеет возможность подключать плагины, читать с некоторым трудом .mht файлы и другие уникальные функции
* [Document Viewer](https://f-droid.org/repository/browse/?fdid=org.sufficientlysecure.viewer) - просмотр множества типов документов.
* [VIMTouch](https://f-droid.org/repository/browse/?fdfilter=vim&fdid=net.momodalo.app.vimtouch) - текстовый редактор, подсветка синтаксисов, старается в некоторой степени следовать стилю компьютерного редактора VIM
* [LibreOffice Viewer for Android](https://www.libreoffice.org/download/android-viewer/) - аналог Microsoft Office, пока еще нестабильный, но уже может работать с многими форматами
* [Collabora Office](https://play.google.com/store/apps/details?id=com.collabora.libreoffice) - основан на LibreOffice, на [сайте LibreOffice](https://www.libreoffice.org/download/android-and-ios/) предлагается установить Collabora Office как замену LibreOffice т.к. версия LibreOffice for Android еще не готова.
* [Editor](https://github.com/billthefarmer/editor) - блокнот с подсветкой синтаксиса для некоторых языков программирования и разметки.
* [Squircle IDE](https://play.google.com/store/apps/details?id=com.blacksquircle.ui) - блокнот с вкладками, автодополнением и подсветкой синтаксиса.
* [Simple Text Editor](https://play.google.com/store/apps/details?id=com.maxistar.textpad) - простой блокнот с некоторыми функциями, т.к. на  Android таких программ мало, то это одна из лучших
* [Librera Reader](https://f-droid.org/en/packages/com.foobnix.pro.pdf.reader/) (Librera PRO) - аналог **Document Viewer**, просмотрщик очень большого количества форматов документов: pdf, djvu, epub, fb2, doc, docx, rtf, mobi etc. Это версия из **F-Droid**, по всей видимости форк PRO версии с большим функционалом, но там отключены все функции связанный с сетью: OPDS, Clouds (Dropbox, Google Drive, Obe Drive) etc.
  * [Librera Reader](https://play.google.com/store/apps/details?id=com.foobnix.pdf.reader) - версия из Google Play, есть все функции, но при этом есть **реклама**
  * [Librera PRO](https://play.google.com/store/apps/details?id=com.foobnix.pro.pdf.reader) - платная PRO версия из Google Play

## Приватность
* [KeePassDroid](https://github.com/bpellin/keepassdroid/releases) - менеджер паролей KeePass
* [Keepass2Android Password Safe](https://play.google.com/store/apps/details?id=keepass2android.keepass2android) - альтернатива **KeePassDroid** с удобным интерфейсом

## Игры

* [RetroArch](http://buildbot.libretro.com/stable/) - эмулятор множества консолей (SEGA, PlayStation и других). Если будут проблемы с интерфейсом - пробуйте старые версии. У этого эмулятора неочевидная настройка джойстика, нужно зайти в настройках в пункт Overlays и выбрать определенный под вашу консоль. **Note.** Есть 2 версии, 32 и 64 битная, их можно установить обе одновременно.
* [PPSSPP](http://www.ppsspp.org/downloads.html) - эмулятор консоли PlayStation Portable

## Система
* **Лаунчеры** (launchers) - графические оболочки
  * [Lawnchair 2](https://play.google.com/store/apps/details?id=ch.deletescape.lawnchair.plah) - одна из немногих качественных, красивых и удобных оболочек. Если в системе используется несколько профилей, то в меню программ будут вкладки для каждого профиля, есть галочка приостановки профиля. Можно настроить для экономии заряда в Amoled дисплее использование настоящего черного цвета.
  * [ADW.Launcher](https://f-droid.org/repository/browse/?fdfilter=launcher&fdid=org.adw.launcher) - альтернативный легкий рабочий стол (launcher), не разрабатывается. **Note.** Не путать с новой версией [ADW Launcher](https://play.google.com/store/apps/details?id=org.adw.launcher) с закрытым кодом!
* **Разное**
  * [Rotation Lock + Landscape](https://f-droid.org/repository/browse/?fdfilter=search&fdid=org.cmotc.tools.rotationlockpp&fdpage=4) - кнопка в шторку для блокировки переворота экрана
  * [Rotation Manager](https://f-droid.org/repository/browse/?fdfilter=rotate&fdid=com.spydiko.rotationmanager_foss) - управление авто поворотом для каждой программы отдельно
  * [Twisted Home Manager](https://f-droid.org/repository/browse/?fdfilter=twisted+home&fdid=com.twsitedapps.homemanager) - менеджер рабочих столов (launcher)
  * [OS Monitor](https://f-droid.org/repository/browse/?fdfilter=OS+Monitor&fdid=com.eolwral.osmonitor) - **работает только на старых версиях Android**, показывает информацию о процессах, соединениях, использовании памяти, процессоре и т. д.
  * [Hacker's Keyboard](https://github.com/klausw/hackerskeyboard/releases) - полноценная клавиатура с ctrl, стрелками, отображением английских символов на русских клавишах и т.д. Словари языков почему-то есть только в Google Play.
  * [DiskUsage](https://f-droid.org/repository/browse/?fdfilter=diskusage&fdid=com.google.android.diskusage) - удобное отображение занимаемого файлами пространства, для удаления ненужных файлов
  * [SD Scanner](https://f-droid.org/en/packages/com.gmail.jerickson314.sdscanner/) - для многих относительно старых систем Android список файлов не обновляется при подключении к компьютеру и новые файлы не видны сразу. Эта программа обновляет состояние.
  * [Save For Offline](https://f-droid.org/packages/jonas.tool.saveForOffline/) - добавляет в меню "поделиться" (share) возможность сохранить файл, например так можно сохранять файлы в Mozilla Firefox
  * [KDE Connect](https://f-droid.org/packages/org.kde.kdeconnect_tp/) - позволяют интегрироваться с KDE (Kubuntu и другими linux системами с KDE) или Gnome (Ubuntu GNOME и др.) с установленным расширением GSConnect. Пересылать файлы, управлять телефоном и прочее по USB кабелю и Wi-Fi сети.
    * **Note.** Если телефон не появляется в списке (не происходит спаривание, pairing), то если у вас linux система нужно разрешить соединение по [этой инструкции](https://userbase.kde.org/KDEConnect#I_have_two_devices_running_KDE_Connect_on_the_same_network.2C_but_they_can.27t_see_each_other), для Ubuntu нужно смотреть раздел **ufw**.
    * **Note.** Версия [KDE Connect в Google Play](https://play.google.com/store/apps/details?id=org.kde.kdeconnect_tp) урезанная из-за правил Google Play. Официальный сайт KDE Connect [тут](https://github.com/GSConnect/gnome-shell-extension-gsconnect/wiki/Installation)
    * **Note.** Для **GNOME** (gtk) есть аналог [GSConnect](https://github.com/GSConnect/gnome-shell-extension-gsconnect/wiki/Installation)
    * **Note.** Оно работает и под Windows. Официальная страница [тут](https://kdeconnect.kde.org/)
    * **Note.** Для просмотра файлов **KDE Connect** на телефоне нужно добавить каталог в список доступных для компьютера. И установить на компьютер программу для работы с протоколом `sft://` для просмотра файлов, e.g. [Cyberduck](https://github.com/iterate-ch/cyberduck) для Windows (в Linux скорее всего уже все установлено).
    * **Note.** Чтобы программа нормально работала нужно дать на телефоне права доступа (SMS, Phone etc) в настройках и возможно отключить оптимизацию расхода заряда батареи.
  * [Island](https://play.google.com/store/apps/details?id=com.oasisfeng.island) - управление **work profile**, для запуска копий программ в изолированной среде, **Shelter** более свободная альтернатива, но не такая удобная.
  * [Shelter](https://play.google.com/store/apps/details?id=net.typeblog.shelter) - запускает программы в изоляции от остальной системы, можно запустить две разных копии одного приложения, например войти в два разных аккаунта Skype одновременно. Создает и использует только один профиль `Work` со слепком программ и сервисов системы (профили кроме основного в новых системах Android можно останавливать и так остановить все приложения в профиле, например в [Lawnchair 2](https://play.google.com/store/apps/details?id=ch.deletescape.lawnchair.plah) в меню программ есть галочка для этого и вкладки с разделением программ по профилям).
    * **Note.** На деле данное приложение только для управления **Work Profile**, для запуска копий программ оно неудобно как и любая программа работающая с **профилями**, для запуска копий программ лучше использовать [Parallel Space](https://play.google.com/store/apps/details?id=com.lbe.parallel.intl) или его аналоги.
  * [J2ME Loader](https://play.google.com/store/apps/details?id=ru.playsoftware.j2meloader) - может запускать программы и игры на java (j2me) от старых телефонов.
  * [Volume Control](https://play.google.com/store/apps/details?id=com.punksta.apps.volumecontrol) - очень удобный регулятор громкости для всех типов звука (будильник, оповещение, bluetooth, наушники, звонок и т.д.).
  * [AlwaysOn | Always On Display](https://play.google.com/store/apps/details?id=io.github.domi04151309.alwayson) - уведомления и разная информация всегда видна на дисплее, для AMOLED дисплее на которых при этом расход заряда минимальный
    * Пока что не очень удобна
  * [NetGuard](https://f-droid.org/ru/packages/eu.faircode.netguard/) - фаервол перехватывающий соединения как локальный прокси. Можно блокировать трафик любой программы кроме системных или подключить фильтр рекламы.
    * Ссылка на версию в **F-Droid** - там больше функций.
    * **NetGuard** может блокировать рекламу, инструкция [тут](https://github.com/M66B/NetGuard/blob/master/ADBLOCKING.md)
  * [Nacho Notch](https://play.google.com/store/apps/details?id=com.xda.nachonotch) - отключает окрашивание в разные цвета в зависимости от открытой программы панель навигации и панель статуса.
    * **Note.** На разных вариантах Android может вести себя по разному или не работать. Чтобы заработало для панели статуса нужно вытащить его иконку в панель статуса!
  * [QuickTiles](https://f-droid.org/en/packages/com.asdoi.quicktiles/) - дополнительный набор иконок для шторки в Android (они называются tile). Кроме иконок на настройки и приложения есть встроенные приложения и иконки для их запуска.
  * [Files](https://play.google.com/store/apps/details?id=com.marc.files) - ярлык для встроенного файл менеджера Android, который имеет доступ к некоторым файлам программ. Не путать встроенную программу Android телефонов **Files** с **Google Files**, который отдельная программа и не имеет дополнительного доступа к файлам софта.
    * **Note.** Программу **Files** можно запустить или из системного меню выбора файлов (file picker), или специальным ярлыком. Просто так из системы запустить его нельзя.
  * [Winlator](https://github.com/brunodev85/winlator) - wine на android для запуска windows софта

## Профессиональные

### Астрономия
* [Sky Map](https://play.google.com/store/apps/details?id=com.google.android.stardroid) и [KStars](https://edu.kde.org/kstars/) - карта звездного неба (созвездия, астрономические данные и т.д.)
* [Look4Sat](https://play.google.com/store/apps/details?id=com.rtbishop.look4sat) - карта искусственных спутников, обновляется через Интернет, показывает в реальном времени.

### Математика
* [microMathematics Plus](https://play.google.com/store/apps/details?id=com.mkulesh.micromath.plus) (μMath+), [GNURoot Octave](https://play.google.com/store/apps/details?id=com.gnuroot.octave) <sup>(для работы нужен root)</sup>, [octave4android](https://github.com/corbinlc/octave4android), [Addi](https://play.google.com/store/apps/details?id=com.addi), [Mather](https://f-droid.org/repository/browse/?fdid=org.icasdri.mather) <sup>(можно писать формулы на Math.js)</sup>  - математические среды для вычислений, построений графиков и прочего. Подойдет студентам и всем кому нужна высшая математика.

### Физика
пусто

### Черчение
пусто

## Программирование, системное администрирование
* [ConnectBot](https://play.google.com/store/apps/details?id=org.connectbot) - SSH клиент.
* [androidVNC](https://f-droid.org/packages/android.androidVNC/) - удаленное управление (например компьютером) по vnc

## Шифрование и изоляции (программ и системы)
* [EDS Lite](https://play.google.com/store/apps/details?id=com.sovworks.edslite) - аналог VeraCrypt, может создавать и открывать крипто контейнеры (шифрованные каталоги) разных форматов: TrueCrypt, VeraCrypt, LUKS etc.

# Программы с открытым кодом для root
## Интернет
* [VPN Hotspot](https://github.com/Mygod/VPNHotspot) - позволяет направлять трафик с точки доступа android через VPN указанный в системе android. В Android нет функции которая может делать это без **root** доступа. На выбор можно направлять трафик из wifi, bluetooth и др. Умеет фильтровать трафик по правилам.
  * **Note.** Чтобы работало на **некоторых** телефонах в меню **Разработчика** нужно отключить аппаратное ускорение модема.
  * **Note.** Страница на [4pda](https://4pda.to/forum/index.php?showtopic=927729)

# Программы с открытым кодом для ADB, Shizuku, Dhizuku
[Android Debug Bridge](https://developer.android.com/tools/adb) (adb) - утилита для Android имеет расширенный доступ, может выполнять некоторые расширенные функции. Недоступна из телефона и обычно нужен только внешний компьютер для выполнения команд adb. [Shizuku](https://github.com/RikkaApps/Shizuku) соединяет телефон с самим собой как внешний компьютер. Поэтому функции adb доступны из самого телефона. **Shizuku** не выполняет никакой работы, а как утилиты **su** для **root** может предоставлять или отбирать доступ к adb api другим программам.

* [Shizuku](https://github.com/RikkaApps/Shizuku) - предоставляет adb api другим программам, может отбирать или давать доступ. Для программ разработанных специально с поддержкой **Shizuku**. Часто программы могут работать в обычном режиме и использовать **Shizuku** если она установлена.
* [Dhizuku](https://github.com/iamr0s/Dhizuku) - непопулярный аналог **Shizuku**
* [awesome-shizuku](https://github.com/timschneeb/awesome-shizuku) - список софта для **Shizuku**
* [aShell](https://f-droid.org/packages/in.sunilpaulmathew.ashell/) - командная строка для команд adb для **Shizuku**

# Программы с закрытым кодом

## Предупреждение
О [Yandex.Store](https://store.yandex.by/). Был случай когда одно из его приложений записывало все окружающие разговоры. С осторожностью относитесь к его программам. После [Google Play](https://play.google.com/store) на сегодня самый большой **официальный** источник программ для Android - это [Amazon Appstore](https://www.amazon.com/getappstore)

## Источники программ (магазины, репозиторий)
* [Google Play](https://play.google.com/store) - самый большой магазин. Код проходит некоторую автоматическую проверку.
* [Yandex.Store](https://store.yandex.by/) - попытка Yandex сделать аналог [Google Play](https://play.google.com/store). Приложений мало, много поддельных, проверка на вредоносность хуже. Достоверно обновляются и надежны только программы от самого Yandex. Прямая ссылка [тут](https://m.soft.yandex.ru/download/appstore/appstore-android.apk)
* [Amazon Appstore](https://www.amazon.com/getappstore) - магазин от [amazon.com](http://amazon.com), много поддельных программ. Трудно искать оригинальные.
* [APKMirror](http://www.apkmirror.com) - **не рекомендуется**. Онлайн сервис через который можно скачать .apk загруженные в него с Google Play. Не имеет репутации, может быть опасен.

## Медиа
* [Google Photos](https://play.google.com/store/apps/details?id=com.google.android.apps.photos) - управление и редактирование фотографий.
* [Shazam](https://play.google.com/store/apps/details?id=com.shazam.android) - по звуку через микрофон определяет исполнителя музыки, может искать по тексту песни и названию альбома.
* **[Adobe](https://play.google.com/store/apps/dev?id=4734916851270416020) и его продукты** - множество программ, в основном для работы с графикой и фото
  * [Adobe Photoshop Express:Photo Editor Collage Maker](https://play.google.com/store/apps/details?id=com.adobe.psmobile)
  * [Adobe Illustrator Draw](https://play.google.com/store/apps/details?id=com.adobe.creativeapps.draw) - для рисования
  * [Adobe Photoshop Sketch](https://play.google.com/store/apps/details?id=com.adobe.creativeapps.sketch) - рисование
  * [Adobe Photoshop Mix - Cut-out, Combine, Create](https://play.google.com/store/apps/details?id=com.adobe.photoshopmix) - эффекты и редактирование для фото
  * [Adobe Capture : Color, Font and Vector Camera](https://play.google.com/store/apps/details?id=com.adobe.creativeapps.gather) - эффекты и редактирование для фото, создание узоров

## Камера
* [Google Camera Port](https://www.celsoazevedo.com/) (она же Pixel Camera Port) - **НЕОФИЦИАЛЬНАЯ** `(БЕЗ гарантий БЕЗОПАСНОСТИ и стабильности!!!)` адаптация [Pixel Camera](https://play.google.com/store/apps/details?id=com.google.android.GoogleCamera) (Google Camera), перепакован и модифицирован оригинальный `.apk` файл, добавлены конфигурации для других не Google Pixel моделей телефонов.
  * **Note.** Есть [Gcam Services Provider](https://github.com/lukaspieper/Gcam-Services-Provider) позволяет запустить **Pixel Camera** без **Google Play Service**, в отличии от **microG** не требует дополнительных прав доступа и запускает только **Pixel Camera**
* [PhotoScan](https://play.google.com/store/apps/details?id=com.google.android.apps.photos.scanner) - позволяет использовать фотоаппарат как фото сканер.
* [Adobe Scan: PDF & Business Card Scanner with OCR](https://play.google.com/store/apps/details?id=com.adobe.scan.android) - может делать сканирование документов и выполнять OCR (оцифровку сфотографированного текста в электронный документ)
* [Cardboard Camera](https://play.google.com/store/apps/details?id=com.google.vr.cyclops) - от Google, позволяет снять VR-фотографии.

## Навигация
* [Yandex.Metro](https://play.google.com/store/apps/details?id=ru.yandex.metro) - навигация по веткам метро для: Москвы, Санкт-Петербурга, Киева, Харькова, Минска и Стамбула и других
* [Yandex.Maps](https://play.google.com/store/apps/details?id=ru.yandex.yandexmaps) - карты от yandex.
* [Google Maps](https://play.google.com/store/apps/details?id=com.google.android.apps.maps) - карты от Google с возможностью скачивать области для offline навигации.
* [Google Earth](https://play.google.com/store/apps/details?id=com.google.earth) - качественные изображения Земли со спутника
* [Google Street View](https://play.google.com/store/apps/details?id=com.google.android.street) - карта с просмотром 3D моделей городов и фото и видео выкладываемого пользователями

## Встроенные и подключаемые устройства

## Интернет
* **Менеджеры загрузок, ftp клиенты, торрент клиенты**
  * пусто
* **Мессенджеры**
  * [Skype](https://play.google.com/store/apps/details?id=com.skype.raider) - видео звонки и чат
  * [Viber](https://play.google.com/store/apps/details?id=com.viber.voip) - видео звонки и чат
  * [Telegram](https://play.google.com/store/apps/details?id=org.telegram.messenger) - видео звонки и чат
  * [WhatsApp Messenger](https://play.google.com/store/apps/details?id=com.whatsapp) - видео звонки и чат
  * [Discord - Chat for Gamers](https://play.google.com/store/apps/details?id=com.discord) - видео звонки и чат
  * [Google Duo](https://play.google.com/store/apps/details?id=com.google.android.apps.tachyon) - видео звонки, нацелен на высокое качество видео даже с плохим сигналом
  * [Hangouts](https://play.google.com/store/apps/details?id=com.google.android.talk) - видео звонки и чат, ограничение на количество человек в групповом звонке
  * [Slack](https://play.google.com/store/apps/details?id=com.Slack) - развитый мессенджер в котором можно создавать под темы сообщений, есть звонки и много других функций. В бесплатной версии ограничение на 10 000 сообщений, нет групповых звонков и невозможность демонстрации экрана.
* **Социальные сети и сервисы**
  * [Twitter](https://play.google.com/store/apps/details?id=com.twitter.android)
  * [Periscope](https://play.google.com/store/apps/details?id=tv.periscope.android) - сервис **Periscope** объявлен устаревшим
  * [YouTube](https://play.google.com/store/apps/details?id=com.google.android.youtube)
* **Почтовые клиенты**
  * пусто
* **Браузеры**
  * [Google Chrome](https://play.google.com/store/apps/details?id=com.android.chrome) - браузер от Google.
  * [Yandex.Browser](https://browser.yandex.by/mobile) - браузер на основе Chromium, сжимает трафик.
  * [Yandex.Browser Lite](https://play.google.com/store/apps/details?id=com.yandex.browser.lite) - браузер, легковесный вариант Yandex.Browser экономящий батарею и ресурсы.
  * [Vivaldi](https://play.google.com/store/apps/details?id=com.vivaldi.browser) - браузер на основе Chrome, с дополнительным функционалом
  * [Opera](https://play.google.com/store/apps/details?id=com.opera.browser) - один из немногих браузеров с встроенным бесплатным VPN. Имеет встроенный блокировщик рекламы, который умеет кликать по соглашениям сайтов "о куках (cookies)".
    * **Note.** Кроме включения VPN в настройках нужно включить его для поиска. И включить использование VPN всегда, а не только для приватных вкладок.
* **Разное**
  * [Psiphon Pro](https://play.google.com/store/apps/details?id=com.psiphon3) - бесплатный сервис для обхода блокировок, при включении проксирует все программы Android. В бесплатной версии есть ограничения
    * **Note.** Подключенные устройства к точке доступа Wi-Fi из-за особенностей Android не проксируются. Возможно есть способы это обойти.

## Органайзеры и справочники
* [GoldenDict](https://play.google.com/store/apps/details?id=mobi.goldendict.android.free) - словарь, бесплатная версия имеет ограничение по количеству подключенных словарей. Полностью поддерживает словари форматов Lingvo, Babylon, StarDict, Lingoes и Dictd.
  * **Note.** Существует одноименная версия с открытым кодом для обычных компьютеров, но для Android версии код закрыт.
  * Подборка словарей Lingvo для русского (перевод с Fr, De, It, Es) **включая** озвучивание слов и толковый словарь (для установки в GoldenDict просто указать каталог словарей в настройках) https://yadi.sk/d/R9KYaEYJ7utB2
* [Measure - Quick Everyday Measurements](https://play.google.com/store/apps/details?id=com.google.tango.measure) - от Google, для измерение длинны и прочего с помощью камеры.
* [Mathematics](https://play.google.com/store/apps/details?id=de.daboapps.mathematics) - удобная математическая среда
* [Graphing Calculator + Math, Algebra & Calculus](https://play.google.com/store/apps/details?id=us.mathlab.android) - графический калькулятор с множеством функций, составление формул, графики и прочее, от разработчика Mathlab
* [Calculator](https://play.google.com/store/apps/details?id=com.miui.calculator) - инженерный калькулятор от **Xiaomi** со встроенным конвертером велечин и валют (курс валют подгружается из Интернета)
* [ColorNote](https://play.google.com/store/apps/details?id=com.socialnmobile.dictapps.notepad.color.note) - заметки
* [Sound Meter](https://play.google.com/store/apps/details?id=com.gamebasic.decibel) - замер уровня шума с пометками об опасности
* [Sound Meter](https://play.google.com/store/apps/details?id=kr.sira.sound&hl=en) - шумометр, меряет уровень шума. Из-за встроенных фильтров Android подавляющих шум может работать неточно. Таблицу вредности шумов можно найти в интернете, например [тут](http://chchearing.org/noise/common-environmental-noise-levels/)
* [Stopwatch Timer](https://play.google.com/store/apps/details?id=com.hybrid.stopwatch) - удобный таймер, с рекламой, которую можно отключить через **NetGuard**

## Файлы
* [7Zipper](https://play.google.com/store/apps/details?id=org.joa.zipperplus7) или [7Zipper 2.0](https://play.google.com/store/apps/details?id=org.joa.zipperplus7v2) - архиватор. Распаковка zip, alz, egg, tar, tar.gz, tar.bz2, gz, bz2, rar, jar, 7z, lzh поддержка разделенных ZIP архивов (Z01, Z02 ..., zip.001, zip.002 .. ). Работа с архивами 7z, ZIP. Hex-Viewer. FTP-клиент и сервер.
* [ZArchiver](https://play.google.com/store/apps/details?id=ru.zdevs.zarchiver) - архиватор. Создавать архивы: 7z (7zip), zip, bzip2 (bz2), gzip (gz), XZ, tar; Распаковывать архивы: 7z (7zip), zip, rar, rar5, bzip2, gzip, xz, iso, tar, arj, cab, lzh, lha, lzma, xar, tgz, tbz, Z, deb, rpm, zipx, mtz, chm, dmg, cpio, cramfs, img (fat, ntfs, ubf), wim, ecm, arc (freearc); Просматривать содержимое архивов: 7z (7zip), zip, rar, rar5, bzip2, gzip, xz, iso, tar, arj, cab, lzh, lha, lzma, xar, tgz, tbz, Z, deb, rpm, zipx, mtz, chm, dmg, cpio, cramfs, img (fat, ntfs, ubf), wim, ecm, arc (freearc);
* [Total Commander](https://play.google.com/store/apps/details?id=com.ghisler.android.TotalCommander) - двух панельный файловый менеджер. Может открывать и архивировать zip.

## Оффис
* [WPS Office + PDF](https://play.google.com/store/apps/details?id=cn.wps.moffice_eng) - офис, для .docx, .xls и других форматов
* [Microsoft Office](https://play.google.com/store/apps/dev?id=6720847872553662727) - набор стандартных офисных программ от Microsoft, для .docx, .xls, презентаций и других форматов по аналогии с офисом для обычных компьютеров.
* [Docs](https://play.google.com/store/apps/details?id=com.google.android.apps.docs.editors.docs), [Slides](https://play.google.com/store/apps/details?id=com.google.android.apps.docs.editors.slides), [Sheets](https://play.google.com/store/apps/details?id=com.google.android.apps.docs.editors.sheets) - набор офисных программ от Google
* [Adobe Fill & Sign: Easy PDF Doc & Form Filler.](https://play.google.com/store/apps/details?id=com.adobe.fas) - позволяет открывать документы и подписывать их жестом по экрану

## Услуги
* [Yandex Go](https://play.google.com/store/apps/details?id=ru.yandex.taxi) - Яндекс Такси, Доставка, Драйв, Продукты, Еда.

## Система
* [Volume control - Vollynx](https://play.google.com/store/apps/details?id=com.loadlynx_jp.vollynx.free) - очень удобный регулятор громкости для всех типов звука (будильник, оповещение, bluetooth, наушники, звонок и т.д.).
* [Rotation Control](https://play.google.com/store/apps/details?id=org.crape.rotationcontrol) - удобное управление вращением экрана и запретами вращения из шторки телефона.
* [Chrome Remote Desktop](https://play.google.com/store/apps/details?id=com.google.chromeremotedesktop) - пара к такой же программе установленной на компьютере, позволяет через интернет управлять телефоном удаленно.
* [Google Cloud Print](https://play.google.com/store/apps/details?id=com.google.android.apps.cloudprint) - управление виртуальными принтерами, в том числе подключенным к облачным сервисам в сети.
* [Quick Settings](https://play.google.com/store/apps/details?id=it.simonesestito.ntiles) - дополнительный набор иконок для шторки в Android (они называются tile). Кроме иконок на настройки и приложения есть встроенные приложения и иконки для них: **Coffeine** (запрет на засыпание телефона), **Rotate** (режимы поворота), **Internet Speed** (статистика по скорости интернета), **Music Volume** (для быстрого отключения звука медиа приложений) и другие.
* [SystemUI Tuner](https://play.google.com/store/apps/details?id=com.zacharee1.systemuituner) - настройка интерфейса системы, скрытие некоторых иконок статус бара, настройка различных уведомлений, действий на кнопки и жесты и прочее. Для полной установки **требуется** через консоль компьютера по USB кабелю в режиме debug отправить телефону команду разрешив **SystemUI Tuner** доступ к некоторым функциям (подробно описано на странице программы в **Google Play**).
* **Benchmarks & Info**
  * [AIDA64](https://play.google.com/store/apps/details?id=com.finalwire.aida64) - инфа о оборудовании и системе, и тест производительности
  * [CPU-Z](https://play.google.com/store/apps/details?id=com.cpuid.cpu_z) - инфа о оборудовании и системе, в овновном и CPU и датчиках
  * [3DMark](https://play.google.com/store/apps/details?id=com.futuremark.dmandroid.application)
* [Power menu controls](https://play.google.com/store/apps/details?id=balti.powermenu.shortcuts) - в новых версиях Android если держать кнопку питания появляется меню в которое кроме кнопок питания можно добавлять другие кнопки, эта программа редактирует это меню.

## Программирование, системное администрирование
Пусто

## Шифрование и изоляции (программ и системы)
* [Gallery Vault](https://play.google.com/store/apps/details?id=com.thinkyeah.galleryvault) - можно прятать фото и видео отправляя в этот контейнер, или возвращать назад в тот же каталог в котором файл был раньше. Вход защищен паролем.
  * **Note.** В программе много рекламы.

# Советы, заметки и решение проблем
## Чего не хватает в этом списке
1. Нормального текстового редактора с подсветкой кода и вкладками
2. Таймера с большим количеством настроек
3. Словаря с поддержкой словарей Lingvo, но без ограничений на количество словарей

## Почему ссылки на **F-Droid** в последнюю очередь?
Ссылки даны на официальные сайты. Если прямых ссылок на ```.apk``` на официальном сайте нет, то ссылка на [Google Play](https://play.google.com/store). Если в [Google Play](https://play.google.com/store) нету, то ссылка на [F-Droid](https://f-droid.org). Если на официальном сайте ссылка на ```.apk``` ведет на другой сторонний источник (магазин), то ссылка на него.

Почему приоритет у **Google Play**, а не у **F-Droid**? Потому что в **F-Droid** обновления задерживаются, а программы бывает пересобираются с ошибками. И потому что из **Google Play** можно выкачать программу напрямую через программу [Yalp Store](https://f-droid.org/en/packages/com.github.yeriomin.yalpstore/) или [Aurora Store](http://auroraoss.com/)

## Почему может не работать авто синхронизация в некоторых программах (например уведомления о почте в **K9 Mail**)?

**Возможно уже неактуально** - `K-9 Mail`сильно переписали, после отказа **Gmail** от старого соединения без поддержки нового типа шифрования.

Некоторые программы, например [K-9 Mail](https://github.com/k9mail/k-9/releases) и его **авто синхронизация**, включают синхронизацию только если синхронизация включена в настройках сети Android. Чтобы все работало **авто синхронизацию** нужно включить в настройках Android, и в пункте **Учетные записи** отключить синхронизации для каждой ненужной программы **отдельно**.

**Note.** На самом деле в настроках сети в [K-9 Mail](https://github.com/k9mail/k-9/releases) можно включить авто синхронизацию всегда даже если в Android она выключена.

Пункт в настройках Android под названием `Счетчик данных > Ограничить фоновые данные` тоже может заблокировать синхронизацию.

Различные оптимизации батареи - это встроенные программы в Android которые могут отключать или замедлять работу других программы, которые по их мнению расходуют много заряда. Нужно поискать в меню Android все что связано с батареей и отключить оптимизации.

У почтовых сервисов есть алгоритмы определения вредоносной дейтельности. Они есть в том числе у сервисов: Gmail, Outlook, Protom Mail и почти **у всех других популярных** неважно **"приватные"** они или нет. Аккаунт **может быть заблокирован в любой момент** как только алгоритму что-то покажется подозрительным. Обычно разблокировать его можно только через сайт сервиса.

**Note:** в разных версиях Android названия пунктов меню и настройки могут отличаться.

**Note.** (не проверено) Возможно **K9 Mail** не поддерживает **push** уведомления, т.е. вместо подписки на сигнал **почта пришла** использует ручное сканирование ящика, что тратит ресурсы и трафик.

## Почему могут не появляться уведомления (notifications) от **K9 Mail** и других программ? Почему некоторые программ работают с задержкой или не правильно работают в фоне (на срабатываю или завершаются)? Что делать? (Это из-за оптимизатора батареи, он их тормозит и завершает)

Чтобы сэкономить заряд в Android есть программы оптимизирующие батарею. Они тормозят и/или завершают программы которые по их мнению расходуют батарею слишком сильно. Они могут не давать показывать им уведомления или не давать срабатывать иногда когда экран телефона погашен или пользователь ими не пользуется (в фоне). В разных версиях Android меню и параметры **разные**, поэтому нужно найти в **настройках** телефона все что связано с батареей и поискать все возможные настройки **оптимизации батареи** и отключить их для всех или для нужных программ.

**Пример для Android 9 Pie:**
1. Configuration > Battery > Battery Saver - **выключить**
2. Configuration > Battery > Adaptive Battery - **выключить**
3. Configuration > Battery > `Triple dot menu` > Battery usage > `Triple dot menu` > Show full device usage - появится список программ в котором нужно выбрать нужную программу, найти там пункт Battery optimization и **отключить** оптимизацию для конкретной программы

## Почему могут не работать сторонние будильники и таймеры?
**Внимательно** проверяйте работу сторонних таймеров, будильников etc, случаи когда производитель меняет настройки системы так, что они иногда не срабатывают были и есть! 

Доверяйте только **родным** будильнику и таймеру!

В основном причина их неправильной работы в том, что Android в целях экономии заряда может завершать или замедлять фоновые процессы. И видимо неродные программы не добавлены в список исключений. Поэтому неродные будильники и таймеры могут работать неправильно.

В новых версиях Android можно отключить "оптимизации энергосбережения" для будильника, таймера etc. В прошивках типа **MIUI** могут быть дополнительные настройки для этого помимо стандартных.

## Почему может не работать вход в почту (например Gmail) через **K9 Mail** и другие?
В настройках аккаунта Google в разделе Безопасность нужно включить доступ к почте для "Ненадежные приложения". Это безопасно.

Не смотря на включенный доступ к "Ненадежные приложения" Google все равно может запросить подтверждение телефона при входе, если посчитает, что это нужно для безопасности даже если вы отключили **2-Step Verification**. Для подтверждение телефона нужно войти в аккаунт через браузер.

Для обхода проблем с **2-Step Verification** привяжите другой почтовый ящик к Gmail, который не будет блокировать при входе в Gmail из другой страны. И используйте его для восстановления доступа к Gmail.

# Слишком громкий или тихий звук при звонке в Android
Такое встречается на некоторых телефонах и прошивках.

**Как исправить:**
1. Через инженерное меню, это работает не на всех телефонах
2. По инструкции через сторонний софт [тут](https://mobileinternist.com/fix-android-volume-too-loud-on-the-lowest-setting)

## MicroG. Почему без Google сервисов не работает навигация и другие функции в некоторых программах?

[MicroG](https://microg.org/) - это open source реализация сервисов Google (т.е. набора библиотек и программ с закрытым кодом), эта реализация может быть не так стабильна как оригинал, но зато обновления к ней могут получить и старые устройства.

Без сервисов Google может перестать работать **большое** количество программ, в том числе open source.

**MicroG** может входить в поставку некоторых альтернативных прошивок (ответвлений от Android).

**Почему может не работать навигация без сервисов Google.** Возможно дело в отсутствии [UnifiedNlp](https://f-droid.org/en/packages/com.google.android.gms/). Он же входит в поставку MicroG. На свежих версия Andoird (например, LinageOS >= 15) UnifiedNlp следует установить как системное приложение (или собрать с патчем, см. [README.md](https://github.com/microg/android_packages_apps_UnifiedNlp/blob/HEAD/README.md)).

## Стандартные приложения Android от Google (в том числе если производитель телефона заменил их своими)
Часто производители телефонов заменяют стандартные приложения Google своими, не всегда лучшими аналогами. Можно установить стандартные версии с Google Play по [этой](https://play.google.com/store/apps/dev?id=5700313618786177705) ссылке.

Чтобы эти приложения работали правильно им нужны специальные права доступа, Google в стандартном Android дает им необходимые права. Установка вручную на измененную производителем телефона систему не всегда позволяет использовать эти приложения нормально. Например **нерадной** вашей системе **будильник** может быть завершен системой для экономии заряда и не сработать.

**Note.** Многие программы из этого списка могут требовать для работы **Google Play Service** или [microG](https://microg.org/) (open source версию **Google Play Service**, не такую стабильную как оригинал). То есть если у вас телефон без этих сервисов (**e.g.** что-то `Huawei` (`Honor`) без их поддержки), то из этого списка может ничего не работать.

**Note.** Некоторые программы Google позволяет устанавливать только определенным устройствам. И **Google Play** может **скрыть** их от вас, даже если они нормально работают у вас.

* [Gboard](https://play.google.com/store/apps/details?id=com.google.android.inputmethod.latin) - стандартная клавиатура, есть возможность ввода свайпом (swipe) не отрывая пальца.
* [Calculator](https://play.google.com/store/apps/details?id=com.google.android.calculator) - стандартный калькулятор.
* [Google Calendar](https://play.google.com/store/apps/details?id=com.google.android.calendar) - стандартный календарь.
* [Contacts](https://play.google.com/store/apps/details?id=com.google.android.contacts) - стандартная программа для управления списком контактов.
* [Синтезатор речи Google](https://play.google.com/store/apps/details?id=com.google.android.tts) (tts) - стандартный синтезатор речи.
* [Clock](https://play.google.com/store/apps/details?id=com.google.android.deskclock) - стандартный набор программ будильника: будильник, таймер, секундомер. **Осторожно,** может не всегда срабатывать, лучше используйте родной будильник системы!
* [Google Рукописный ввод](https://play.google.com/store/apps/details?id=com.google.android.apps.handwriting.ime) (handwriting) - стандартная клавиатура для рукописного ввода.
* [Dialer](https://play.google.com/store/apps/details?id=com.google.android.dialer) (Телефон) - стандартная программа для телефонных звонков.
* [Google Camera](https://play.google.com/store/apps/details?id=com.google.android.GoogleCamera) - для моделей телефонов не от Google может не находиться через **приложение** Google Play, но будет видна через **сайт** Google Play. Скачать можно через [Aurora Store](http://auroraoss.com/) установив там подмену модели телефона на телефон от Google.
* [Gallery Go](https://play.google.com/store/apps/details?id=com.google.android.apps.photosgo) - галлерея

[Research at Google](https://play.google.com/store/apps/developer?id=Research+at+Google) - это набор экспериментальных приложений
* [Live Transcribe & Notification](https://play.google.com/store/apps/details?id=com.google.audio.hearing.visualization.accessibility.scribe)
* [Fabby: Selfie Art Camera](https://play.google.com/store/apps/details?id=com.fabby.android) - эффекты для селфи фото
* [Motion Stills](https://play.google.com/store/apps/details?id=com.google.android.apps.motionstills) - эффекты видео
* [Fabby Look: hair color changer](https://play.google.com/store/apps/details?id=com.fabbyhair.android) - изменение цвета волос

## Про программы камеры для фото и видео и о том что же выбрать
Сейчас на разных телефонах устанавливаются разные наборы камер, с разными функциями, разным количеством камер. Есть большая вероятность что все функции камер телефона будут доступны только в родной камере. Все другие камеры будут иметь только ограниченные функции не смотря на то что в них может быть больше режимов.

Существует **Camera 2 API**, который может быть не доступен не родным камерам. На большинстве нормальных телефонов он доступен всем камерам. На некоторых телефонах его можно включить (сделать доступным) через **custom recovery**. (e.g. функция включения **Camera 2 API** есть в [Sky Hawk Recovery Project](https://shrp.team/))  
Даже если система дает доступ к **Camera 2 API** в настройках некоторых программ нужно включить его напрямую, он может быть отключен потому что неродная камера не знает есть доступ к **Camera 2 API** или нет и не может его использовать по умолчанию (e.g. [Open Camera](https://play.google.com/store/apps/details?id=net.sourceforge.opencamera)).

**Нестандартные** функции камер, e.g. такие как "многопиксельность" (режим **48mp** и выше) могут не поддерживаться стандартными камерами даже если прошивка дает доступ к **Camera 2 API**.

**Этот раздел не закончен, тут еще будет про camera api, мегапиксели и о том что лучше выбрать!**

## Скрытие приложений, фото и прочего в Android
`раздел в процессе редактирования`

**Зачем.** В некоторых странах незаконно могут досматривать телефон. Как правило такие досмотры визуальные, без запуска программ для анализа данных.

**Work profile** `VS` **"Пользователи"** `VS` **Second Space** (Private Space) `VS` **Multi Parallel like applications**. Это совсем разные вещи. Из них только **Second Space** дает реальную защиту. Остальные способы имеют недостатки и что-то похожее на **Second Space** можно получить только с **Multi Parallel like applications**

* **Какие способы сокрытия программ и данных существуют:**
  * Создание нового **"пользователя"** в современных системах Android создает полный изолированный слепок системы. Файлы и программы оттуда не доступны другим пользователям. У созданных пользователей меньше прав чем у основного пользователя, например они не могут создат точку доступа wi fi
  * **work profile** - изолированная среда, файлы из нее недоступны обычным программам или наоборот. Хотя существуют программы прослойки вроде [Shelter](https://play.google.com/store/apps/details?id=net.typeblog.shelter), которые позволяют управлять **work profile** и получать доступ к файлам в обе стороны. в отличии от **"пользователей"** чтобы запустить из нее программы не нужно переключаться на другой слепок системы и программы из **work profile** работают одновременно с основными. Создается/удаляется **work profile** или через настройки системы, или через программы вроде **Shelter**.

* **Список программ и функций для скрытия данных от незаконного анализа**
  * [AppLock](https://play.google.com/store/apps/details?id=com.domobile.applockwatcher) - ставит пароли на любое приложение, свою иконку может замаскировать под чужую. Но скрыть так программу не получится.
  * [Gallery Vault](https://play.google.com/store/apps/details?id=com.thinkyeah.galleryvault) - скрывает фото и видео внутри себя от стандартной Gallery, скрытие работает путем отправки фото через `Share to`, при восстановлении доступа фото можно вернуть на прежнее место. **Note.** У этой программы столько рекламы, что пользоваться им нереально, нужно искать альтернативы.
  * [Guardian Rom](https://www.wilderssecurity.com/threads/guardian-rom-secure-android-os.348416/page-9) - custom rom, устарел и почти не разрабатывается, имеет функцию **HiddenOS**, которая может скрыть реальную систему и открыть ненастоящий слепок системы, если ввести неверный пароль
  * [Secure Folder](https://play.google.com/store/apps/details?id=com.samsung.knox.securefolder) - утилита от Samsung которая создает зашифрованную изолированную среду, работает только для родной прошивки, при использовании custom rom данные станут недоступными. Судя по описанию скрывать данные и наличие зашифрованных каталогов не может
  * [Secure Spaces](https://forum.xda-developers.com/google-nexus-5/development/secure-spaces-android-rom-t3251299) - возможно устревшая прошивка, которая позволяла создавать изолированные слепки системы, на подобии создания современных "пользователей" в Android. Только настроек прав доступа там было больше чем в "пользователях".
  * [Multi Parallel - Multiple Accounts & App Clone](https://play.google.com/store/apps/details?id=multi.parallel.dualspace.cloner) или [Parallel Space](https://play.google.com/store/apps/details?id=com.lbe.parallel.intl) (и [Parallel Space - 64Bit](https://play.google.com/store/apps/details?id=com.lbe.parallel.intl.arm64)) - клонирует любое приложение, склонированная программа не видна в списке программ или на рабочем столе (но можно создать ярлык)
    * **Note.** Все подобные программы ищутся по словам [clone app](https://play.google.com/store/search?q=clone%20app&c=apps)
    * **Note.** Такие программы не создают **"пользователя"**. И не используют **work profile**. Такие программы создают копию `/data` каталога, всех настроек программы, но при этом используют один общий apk файл. Поэтому они не тратят ресурсы системы и им не нужно время на переключение **"пользователей"**.
  * [Private Space](https://consumer.huawei.com/en/support/content/en-us00754246/) - эта фукций в прошивке **EMUI** для устройств Huawei (и Honor). Причем Huawei делает эту функцию доступной не во всех телефонах даже если они имеют одну и ту же версию прошивки. Также может быть доступно в портах EMUI на другие устройства.
    * **Note.** Т.к. прошивка китайская, то вероятность того, что где-то есть суперпароль через который можно разблокировать любое пространство очень высокая.
  * [Second Space](https://c.mi.com/thread-213826-1-0.html) - функция из прошивки **MIUI** аналогична **Private Space**. В отличии от **EMUI** прошивка **MIUI** может быть портирована на другие устройства.
    * **Note.** Т.к. прошивка китайская, то вероятность того, что где-то есть суперпароль через который можно разблокировать любое пространство очень высокая.
    * **Note.** Понять использует ли пользователь **Private Space** просто, в режиме "скрытия" **Private Space** пункт меняю **"Создать Private Space"** исчезнет из обычного режима и будет доступен только внутри **Private Space**. Поэтому убедить условного "атакующего" что пользователь не использует **Private Space** задача пользователя.
  * [Private Mode](http://forum.flymeos.com/thread-17167-1-1.html) - аналог **Private Space**, это функция телефонов **Meizu** с прошивкой **Flyme OS** со своими особенностями.
    * **Note.** Нормально официальной страницы с описанием этого режима не найдено.