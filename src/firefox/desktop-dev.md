#### Что это?

Дополнения и инструменты для программистов, web разработки, разработки для Firefox

Статьи
* [Manifest V3 & Manifest V2](https://blog.mozilla.org/addons/2024/03/13/manifest-v3-manifest-v2-march-2024-update/) - отличия api расширений Firefox от Chrome, и о том что Firefox будет поддерживать manifest v2 с большим функционалом (в Chrome старые некоторые расширения перестанут работать, например некоторые расширения по блокировке рекламы)
* [Manifest V3 migration guide](https://extensionworkshop.com/documentation/develop/manifest-v3-migration-guide/) - миграция на manifest v3

Список библиотек и инструментов
* [Awesome WebExtensions](https://github.com/bfred-it/Awesome-WebExtensions) - набор ссылок на инструменты для разработки дополнений
* [redux-webext](https://github.com/ivantsov/redux-webext) - библиотека для разработки расширений для firefox и chrome, позволяет удобно обмениваться сообщениями между background и content скриптами
* [NativeExt](https://addons.mozilla.org/firefox/addon/native-ext/) - для настольных версий Firefox предоставляет API для работы с файлами системы. Для работы нужно установить отдельную программу
* [Deprecated tools](https://developer.mozilla.org/en-US/docs/Tools/Deprecated_tools) - список функций для разработки, который когда-то были в Firefox, но были удалены, для каждой из таких функций указан альтернативный вариант часто это расширения которые теперь можно установить отдельно
* [Content Security Policy parser](https://github.com/helmetjs/content-security-policy-parser) - парсер CSP для подмены http headers чтобы включить некоторые функции, это важно т.к. есть проблема использования расширений на страницах с жесткими правилами CSP и их нужно частично отключать (как делает например uBlock Origin)
* [css-tree](https://www.npmjs.com/package/css-tree) и [css](https://github.com/reworkcss/css) - css парсеры, т.к. просто так изменить css нельзя, не все значения можно извлечь из перечисленных в css свойстве - нет встроенныъ js функций для этого
* [ublock origin sanitize csp](https://github.com/gorhill/uBlock/issues/3140) - ublock origin блокирует csp и вызывает сообщения в браузере об этом, тут обьяснение почему (без этого никак)
  * Note. Если писать расширение которое делает inject скриптов на страницу (а не просто выполняет content скрипт изолированно), то возможно придется чистить csp точно также.

Список расширений
* [Extension source viewer](https://addons.mozilla.org/en-US/firefox/addon/crxviewer/) - можно посмотреть код дополнения Firefox прямо на странице его установки на addons.mozilla.org
* [xPath Finder](https://addons.mozilla.org/ru/firefox/addon/xpath_finder/)
* [Try XPath](https://addons.mozilla.org/ru/firefox/addon/try-xpath/)
* [RESTer](https://addons.mozilla.org/ru/firefox/addon/rester/)
* [Open Pesticide](https://addons.mozilla.org/en-US/firefox/addon/open-pesticide/)
* [Display #Anchors](https://addons.mozilla.org/en-US/firefox/addon/display-_anchors/) или [Anchors reveal](https://addons.mozilla.org/ru/firefox/addon/anchors-reveal/)
* [Web Developer](https://addons.mozilla.org/en-US/firefox/addon/web-developer/)
* [aXe Developer Tools](https://addons.mozilla.org/en-US/firefox/addon/axe-devtools/)
* [DevTools Media Panel](https://addons.mozilla.org/en-US/firefox/addon/devtools-media-panel/)
* [WebSocket Detector](https://addons.mozilla.org/en-US/firefox/addon/websocket-detector/)
* [WebAssembly Detector](https://addons.mozilla.org/en-US/firefox/addon/webassembly-detector/)
* [WebSitePulse Test Tools ](https://addons.mozilla.org/en-US/firefox/addon/websitepulse-test-tools/)
* [List Sources](https://addons.mozilla.org/en-US/firefox/addon/list-sources/)
* [Quantum Hackbar](https://addons.mozilla.org/ru/firefox/addon/quantum-hackbar/)
* [Empty Cache Button](https://addons.mozilla.org/ru/firefox/addon/empty-cache-button/)
* [devtools-highlighter](https://addons.mozilla.org/ru/firefox/addon/devtools-highlighter/)
* [Live editor for CSS and LESS](https://addons.mozilla.org/en-US/firefox/addon/live-editor-for-css-and-less/) - редактор css и less в браузере, подсветка синтаксиса, автоформатирование, минимизация и др.
* [{find+}](https://addons.mozilla.org/ru/firefox/addon/brandon1024-find) - поиск на странице по Regular Expression
* [Augury](https://addons.mozilla.org/en-US/firefox/addon/angular-augury/) - инструменты для Angular разработчика
* [React Developer Tools](https://addons.mozilla.org/en-US/firefox/addon/react-devtools/) - инструменты для React + Redux разработчика
* [Google Lighthouse](https://addons.mozilla.org/en-US/firefox/addon/google-lighthouse/) - от Google, для теста и анализа сайтов
* [Content Security Policy (CSP) Generator](https://addons.mozilla.org/ru/firefox/addon/csp-generator/) - генератор CSP правил