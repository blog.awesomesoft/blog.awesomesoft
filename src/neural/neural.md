* типы моделей нейронных сетей
  * gpt
  * LLM
  * diffusion
  * RNN - e.g. Marian, Bergamot
  * трансформеры
* базовые понятия ai
  * API Key - логин и пароль в виде токена, доступен в настройках разных чат ботов, его нужно использовать как в настройках клиентов чат ботов чтобы отправлять запросы через альтернативные клиенты. Часто api key платный.
  * multi module ai - когда разные типы ai для генерации и распознавания разных типов данных работают совместно и передают друг другу данные и результаты
  * Artificial general intelligence (AGI) - гипотетически возможная нейросеть, настоящий искусственный интеллект который умеет думать как человек
  * reasoning - симуляция логики через статистику, имитация осознания смысла и человеческую логику через статистику, устанавливает связи между данными, имитирует Дедукция, Индукцию, Причинно-следственные связи
    * Паттерны и ассоциации - статистическая связь между словами (похожими или подходящими по смыслу)
    * Контекстное предсказание - предсказание следующего слова по контексту
    * Имитация логики - по шаблону решают многошаговые задачи
  * Chain-of-Thought (CoT) - метод reasoning где модель «проговаривает» шаги решения что повышает точность
  * Fine-tuning - метод reasoning, Дополнительное обучение на узкоспециализированных данных (например, математических задачах).
  * Instruments - метод reasoning, Интеграция с калькуляторами, базами знаний или кодом для проверки фактов.
  * квантование - обрезание длинны весов в виде дробных чисел до более коротких после запятой, уменьшает потребляемую память моделью, делает ее более глупой (если различия в весах небольшие, то модель уже не может определить в какую ветку нейросети идти т.к. малые значения обрезаны и идет в случайную)
  * дистилляция - когда одна модель учит другую (одна задает вопросы, вторая отвечает), большая сложная модель может так обучить меньшую и при этом меньшая может быть умнее чем большая (зависит от методов обучения)
  * crawler - сканер сети и др ресурсов которые используют ai для скана ресурсов, иногда создают ддос ресурсов которые сканируют и игнорируют правила robots.txt
  * скраппинг - процесс чтения crawler'ами данных с сайтов и др ресурсов
  * tarpitting - борьба с ai сканерами и спам программами путем подсовывания им фейк ссылок и ресурсов
  * отравление crawler - если скормить краулеру фейковые данные для обучения модели, то это внесет фековые данные в нее
  * HPR, Helpful Rate - коэффициент успешной обработки запроса или уровень успешного обхода фильтров, если про обход цензуры
  * HS, Harmfulness Score - балы вредоносности ответа модели, например если он рассказывает как сделать что-то запрещенное
  * ASR, Attack Success Rate - балы успешности атаки на модель, например для обхода цензуры
  * PPL, Paraphrase и Retokenization - методы защиты фильтров, т.е. методы цензурирования
  * AI Assistant - пред настроенные AI модели, например модель может играть роль секретаря или ученого, также в настройках может быть включен jail break который обходит цензуру. На сайтах и сервисах можно создавать такие модели путем задания пред настроенного начального запроса для модели и выбором модели.
    * Note. В правилах создания AI Assistant можно задать обращение к внешним ресурсам, напр. за доп инфой.
* anti censor gpt (джейлбрейк) - текст для убеждения gpt обойти цензуру, работает путем изменения весов нейросети, т.к. цензура gpt делается путем задания правил когда веса зацензурированной информации низкие и вероятность ее вывода низкая
  * [BlackFriday-GPTs-Prompts](https://github.com/friuns2/BlackFriday-GPTs-Prompts/tree/main)
  * [ChatGPT_DAN](https://github.com/0xk1h0/ChatGPT_DAN)
  * [ChatGPT-Dan-Jailbreak.md](https://gist.github.com/coolaj86/6f4f7b30129b0251f61fa7baaa881516)
  * [ArtPrompt](http://arxiv.org/pdf/2402.11753) - обход через текст в виде ASCII-картинок
  * [jailbreak_llms](https://github.com/verazuo/jailbreak_llms)
  * [bon-jailbreaking](https://github.com/jplhughes/bon-jailbreaking)
  * [Time Bandit](https://cybersecuritynews.com/chatgpt-4o-jailbreak-vulnerability/) - атака при которой gpt убеждают что сейчас другой год или другое время для пользователя, что отключает фильтры
* софт для загрузки и запуска моделей локально
  * [gpt4all](https://github.com/nomic-ai/gpt4all) - приложение с графическим интерфейсом для работы с gpt моделями и их загрузки из ремозитория
  * [Open WebUI](https://github.com/open-webui/open-webui) - ui для работа с загруженными моделями локально
  * [Ollama](https://github.com/ollama/ollama) - утилита командной строки для загрузки разных gpt моделей из репозитория и запуск
  * [Gradio](https://github.com/gradio-app/gradio) - встраиваемый ui который может поставляться вместе с моделью
  * [Stable Diffusion web UI](https://github.com/AUTOMATIC1111/stable-diffusion-webui) - ui для работы с генерацией графики через разные загруженные модели
  * [Chatbox AI](https://chatboxai.app) - клиент чат ботов, т.е. GUI чата для трансляции запросов разным моделям, в основном по сети (т.е. интегрируется с api чат ботов). Есть управление преднастоенными правилами (напр. ). Можно купить подписку от Chatbox AI. Есть веб версия https://web.chatboxai.app/
* ai crawler - софт которые не дает сканерам ресурсов сети обучающим ai получать доступ к ресурсам, подменяет ссылки на ресурсы и создает фейковые ссылки и ресурсы, отдавая сканерам ai мусорные данные. Изначально созданы т.к. ai сканеры сети игнорируют правила robots.txt
  * [Nepenthes](https://zadzmo.org/code/nepenthes/)
  * [Iocaine](https://iocaine.madhouse-project.org/)
  * [Quixotic](https://marcusb.org/hacks/quixotic.html)
* нейросети разбитые по производителям
  * Adobe
    * [Adobe Firefly](https://firefly.adobe.com/inspire/images)
  * Microsoft
    * [Microsoft Copilot](https://copilot.microsoft.com)
    * [Microsoft Designer](https://designer.microsoft.com/)
  * Tencent
    * [Hunyuan3D 2.0](https://github.com/Tencent/Hunyuan3D-2)
  * Midjourney
    * [Midjourney](https://www.midjourney.com/home)
  * OpenAI
    * ChatGPT
    * [DALL-E]()
  * Stability AI
    * [Stable Diffusion]()
    * Stable Video Diffusion
    * Stable Audio
    * Stable Point Aware 3D
    * Stable Fast 3D
    * Stable Video 3D
    * Stable Video 4D
    * Stable Zero 123
    * Stable TripoSR
    * Stable LM
    * Stable Code
    * Stable LM Zephyr
    * Stable Beluga
  * Udio
    * [Udio]()
  * Google / Google DeepMind
    * Gemini (Bard)
  * Wolfram Research
    * [WolframAlpha](https://www.wolframalpha.com/)
  * DeepSeek
    * DeepSeek R1
    * Janus-Pro-7B
  * Meta
    * LLaMA
  * Nvidia
    * NVLM-D
  * Alibaba
    * Qwen
  * Mistral AI
    * [Mistral-Nemo-Instruct-2407](https://huggingface.co/mistralai/Mistral-Nemo-Instruct-2407)
    * [Pixtral-12B-Base-2409](https://huggingface.co/mistralai/Pixtral-12B-Base-2409)
  * Anthropic
    * Claude
  * Amazon
  * Apple
  * [Hugging Face](https://huggingface.co)
    * [Open R1](https://github.com/huggingface/open-r1) - открытая реализация DeepSeek R1
  * Yandex
    * Alice (YandexGPT)
    * Shedevrum (YandexART)
  * Sber
    * GigaChat
    * Kandinsky
* Анонимные хакерские модели
  * WormGPT
  * FraudGPT
* сервисы для работы с ai
  * https://lmarena.ai/ - арена теста чат ботов, доступны бесплатно и без регистрации много ботов, но они урезанные, см вкладку Direct Chat
  * https://inference.cerebras.ai - сайт производителя NPU где для теста запущено несколько быстро работающих моделей
  * https://huggingface.co/chat/ - набор моделей от huggingface которые можно бесплатно использовать онлайн, но они запущены с ограничением по скорости работы