Source: [1](https://www.kernel.org/doc/html/latest/admin-guide/mm/ksm.html), [2](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/virtualization_tuning_and_optimization_guide/chap-ksm)

**KSM** (Kernel same-page merging) - merges the same RAM pages

**Why?** Less CPU, more RAM memory. When run applications with the same chunks of code or when run KVM or another virtualization.

**Cons?** Additional CPU performance is used to merge RAM pages.

```sh
# check if enabled
cat /sys/kernel/mm/ksm/run

# enable
echo 1 > /sys/kernel/mm/ksm/run

# disable
echo 0 > /sys/kernel/mm/ksm/run

# check KSM configuration
grep -H '' /sys/kernel/mm/ksm/*
```