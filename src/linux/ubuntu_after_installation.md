**What is it?** It is list of actions that you need to do after **Ubuntu 22.04** or **Kubuntu 22.04** installation.

- [Ubuntu information \& news](#ubuntu-information-news)
- [Useful commands](#useful-commands)
- [Awesome Links](#awesome-links)
- [Rules](#rules)
- [User soft](#user-soft)
  - [Soft](#soft)
  - [Official PPAs only](#official-ppas-only)
  - [Custom official repository or no repository](#custom-official-repository-or-no-repository)
  - [KDE](#kde)
  - [Official KDE PPAs only](#official-kde-ppas-only)
  - [Essential KDE setting configuration](#essential-kde-setting-configuration)
- [Alternative Kernel with configuration and patches](#alternative-kernel-with-configuration-and-patches)
- [OS disk image like applications](#os-disk-image-like-applications)
- [Development](#development)
  - [Soft and repositories](#soft-and-repositories)
  - [Custom repository or no repository](#custom-repository-or-no-repository)
- [Recipes](#recipes)
  - [How to fix broken packages?](#how-to-fix-broken-packages)
  - [What is HWE (Hardware Enablement Stacks)? How to upgrade kernel and other to current version?](#what-is-hwe-hardware-enablement-stacks-how-to-upgrade-kernel-and-other-to-current-version)
  - [Unable to get list of updates: Failed to update metadata for lvfs](#unable-to-get-list-of-updates-failed-to-update-metadata-for-lvfs)
  - [Fix for a bug when WSLView try to open any file instead of be opened by target application](#fix-for-a-bug-when-wslview-try-to-open-any-file-instead-of-be-opened-by-target-application)
  - [Install drivers](#install-drivers)
  - [Install discrete nvidia video card driver and run an application](#install-discrete-nvidia-video-card-driver-and-run-an-application)
  - [Disable and uninstall `apport`](#disable-and-uninstall-apport)
  - [Possible migration from `libinput` touchpad driver to `synaptics` driver](#possible-migration-from-libinput-touchpad-driver-to-synaptics-driver)
  - [Right way to remove PPA repository](#right-way-to-remove-ppa-repository)
  - [How to run bash script as sudo on Ubuntu start up?](#how-to-run-bash-script-as-sudo-on-ubuntu-start-up)
  - [Fix `/home` directory file rights](#fix-home-directory-file-rights)
  - [Disable KDE available updates notification](#disable-kde-available-updates-notification)
  - [Optional. Remove snap](#optional-remove-snap)
  - [Touchpad control](#touchpad-control)
- [Useful commands](#useful-commands-1)
  - [KDE](#kde-1)
  - [Device control](#device-control)

# Ubuntu information & news
* [kubuntu news](https://kubuntu.org/news/)
* [kde news](https://dot.kde.org/)
* [KDE Applications](https://apps.kde.org/) - list of all official KDE Applications, not all of them are installed in Kubuntu
* [Kubuntu/PPAs](https://community.kde.org/Kubuntu/PPAs#Kubuntu_Updates) (with backports) - **outdated** page for now!
* [phoronix KDE news](https://www.phoronix.com/linux/KDE)
* [omgubuntu](https://www.omgubuntu.co.uk/)

# Useful commands
* [The 5-Minute Essential Shell Tutorial](https://community.linuxmint.com/tutorial/view/100)
* [Utilities / Tools for Examining Your System State](https://community.linuxmint.com/tutorial/view/2109)
* [Linux Terminal Command Reference](https://community.linuxmint.com/tutorial/view/244)

# Awesome Links

* [Awesome-Linux-Software](https://github.com/luong-komorebi/Awesome-Linux-Software)
* [awesome-linuxaudio](https://github.com/nodiscc/awesome-linuxaudio)

# Rules
1. Use repositories if possible, it will reduce amount of libraries and memory consumption
2. Use official PPAs or other repository only for security reasons, it means you can use only PPAs from official sites that name these PPAs "official" on its pages.
3. Try to do not use snap repository, they have restrictions and a lot of applications can not work normally, Also they work slow and consume a lot of memory.
4. If there are no needed application everywhere except in snap or flatpak repository you can use snap or flatpak on your own risk.
5. If you can not install `.deb` from GUI utility then try to use command line: `sudo dpkg -i /home/user/Downloads/mysoft.deb`

# User soft

## Soft
```sh

# command line utilities
# NOTE: some command line soft MAY have GUI!
sudo apt install neofetch ncal procinfo finger fdupes rdfind rmlint curl aria2 ppa-purge xterm smartmontools dmsetup mc inxi evtest testdisk wipe dnscrypt-proxy powertop ffmpeg tilde sshpass htop gnupg lsb-release ca-certificates grub-emu openvpn exfatprogs davfs2 python-is-python3 dosbox kpcli handbrake-cli net-tools psmisc lsof

# command line file archivers and converters
sudo apt install android-sdk-libsparse-utils e2fsprogs p7zip-full unar zlib1g-dev liblzo2-dev lzop lziprecover libhyperscan-dev zstd liblz4-tool ccd2iso nrg2iso mdf2iso cdi2iso pdi2iso b5i2iso alien

# command line runtimes, frameworks and environments to run soft (C# .NET, Java JVM, Python and other)
sudo apt install mono-complete

# for GTK:
#   doublecmd-qt > doublecmd-gtk
#   network-manager-openvpn-gnome
sudo apt install keepassxc inkscape goldendict convertall uget filezilla kubuntu-restricted-extras qbittorrent handbrake simplescreenrecorder remmina peek mpv smplayer smplayer-themes geany geany-plugins gparted vlc calibre doublecmd-qt network-manager-openvpn flameshot libnotify-bin vulkan-tools notepadqq darktable lutris stellarium gimp gimp-data-extras gimp-plugin-registry rawtherapee shotcut audacity blender flowblade grafx2 freecad librecad playonlinux xchm scribus scribus-template texmaker shotwell acetoneiso glogg mypaint obs-studio obs-cli obs-scene-collection-manager obs-text-slideshow obs-downstream-keyer obs-scene-notes-dock obs-text-slideshow obs-advanced-scene-switcher pulseeffects webcamoid rmlint-gui smb4k labplot

# optionally! enable kernel version auto upgrade
# WARNING! You may need to reinstall some drivers after reboot! E.g. Nvidia proprietary driver.
sudo apt install --install-recommends linux-generic-hwe-22.04
```

## Official PPAs only
```sh
# Ubuntu repository vs PPA versions:
    simplescreenrecorder-lib:i386 => none
    handbrake-gtk => handbrake
    smplayer-skins => none

# Optional!!! Do not install if you want stability!
# Official Kubuntu KDE backports
# You may need "kubuntu-ppa/backports" and "kubuntu-ppa/backports-extra" from the list!
# Source: https://www.google.com/search?q=site%3Ahttps%3A%2F%2Fkubuntu.org+"kubuntu-ppa%2Fbackports"
sudo add-apt-repository ppa:kubuntu-ppa/ppa # Optional!!! Newest bugfixes only
sudo add-apt-repository ppa:kubuntu-ppa/backports # Some features from newer releases
sudo add-apt-repository ppa:kubuntu-ppa/backports-extra # Major KDE veersino updates

# keepassxc
sudo add-apt-repository ppa:phoerious/keepassxc

# simplescreenrecorder
sudo apt-add-repository ppa:maarten-baert/simplescreenrecorder
sudo apt install simplescreenrecorder-lib:i386

# inkscape
sudo add-apt-repository ppa:inkscape.dev/stable

# qbittorrent
sudo add-apt-repository ppa:qbittorrent-team/qbittorrent-stable

# smplayer
sudo add-apt-repository ppa:rvm/smplayer
sudo apt install smplayer-skins

# handbrake
sudo add-apt-repository ppa:stebbins/handbrake-releases
sudo apt install handbrake-gtk

# remmina
sudo apt-add-repository ppa:remmina-ppa-team/remmina-next

# peek
sudo add-apt-repository ppa:peek-developers/stable

# notepadqq
sudo add-apt-repository ppa:notepadqq-team/notepadqq

# lutris
sudo add-apt-repository ppa:lutris-team/lutris

# OBS Studio
sudo add-apt-repository ppa:obsproject/obs-studio
sudo apt install ffmpeg obs-studio
```

## Custom official repository or no repository
* [Google Chrome](https://www.google.com/intl/ru/chrome/)
* [Firefox](https://www.mozilla.org/firefox/all/)
* [Thunderbird](https://www.thunderbird.net)
* [Telegram](https://desktop.telegram.org/)
* [Skype](https://www.skype.com/ru/get-skype/)
* [vscode](https://code.visualstudio.com/)
* youtube-dl
    ```sh
    # youtube-dl
    sudo wget https://yt-dl.org/downloads/latest/youtube-dl -O /usr/local/bin/youtube-dl
    sudo chmod a+rx /usr/local/bin/youtube-dl
    ```
* [yt-dlp](https://github.com/yt-dlp/yt-dlp) - fork of youtube-dl with additional functionality
* [spotify-downloader](https://github.com/spotDL/spotify-downloader) - download music from spotify through youtube
  ```sh
  # 1. install
  pip install spotdl
  spotdl --download-ffmpeg
  pip install yt-dlp
  # 2. download artist, for playlist use "{list-name}/{title}.{ext}" template
  spotdl "https://open.spotify.com/playlist/5UoBDyuzzRkaYbVT8Hy0hq" --output "{artists}/{album}_{year}_{disc-number}/{track-number}_{title}" --bitrate 320k --sponsor-block --max-retries 8 --audio youtube-music youtube soundcloud bandcamp piped
  ```
* [WebTorrent](https://webtorrent.io/) - client that support torrent protocol through WebRTC, it does not support classic torrent protocol, so you can download from torrent seeds (sources) with WebRTC clients
* Java https://www.oracle.com/java/technologies/downloads/
* doublecmd https://sourceforge.net/p/doublecmd/wiki/Download/
    ```sh
    # For xUbuntu 20.04 run the following:
    echo 'deb http://download.opensuse.org/repositories/home:/Alexx2000/xUbuntu_20.04/ /' | sudo tee /etc/apt/sources.list.d/home:Alexx2000.list
    curl -fsSL https://download.opensuse.org/repositories/home:Alexx2000/xUbuntu_20.04/Release.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/home_Alexx2000.gpg > /dev/null
    sudo apt update
    sudo apt install doublecmd-qt # for KDE, for GTK use doublecmd-gtk
    ```
* [Audacity](https://www.audacityteam.org/)
* [darktable](https://www.darktable.org/)
* [CDEmu](https://cdemu.sourceforge.io/) - emulate an optical drive and disc (CD-ROMs and DVD-ROMs) to bypass protection or to let use damaged discs
* [VeraCrypt](https://www.veracrypt.fr/code/VeraCrypt/)
* [Tribler](https://github.com/Tribler/tribler) - use networks of community proxies to bypass torrent protocol detection and bloking by internet providers
* Google Earth Pro https://www.google.com/intl/ru/earth/versions/#download-pro
* [Ventoy](https://github.com/ventoy/Ventoy) - makes bootable USB drive
* Blender https://www.blender.org
* HDDSuperClone [1](http://www.hddsuperclone.com/sitev1/), [2](https://github.com/thesourcerer8/hddsuperclone) - rescue data from corrupted HDD, SDD or else, makes virtual disk with copied data sectors to avoid requests to corrupted disk
* [android-udev-rules](https://github.com/M0Rf30/android-udev-rules) - **android adb drivers** to use debug or adb commands or software that use adb with wider range of android devices
* [GameMode](https://github.com/FeralInteractive/gamemode) - optimizes Linux for games (for a single application performance), configures CPU governor, I/O priority, Process niceness, Kernel scheduler, GPU performance mode
  * **Note.** Do not use it for now, it has a lot of bugs. And it for full functionality you has to install custom linux kernel with additional CPU Scheduler.
* [Nyr/openvpn-install](https://github.com/Nyr/openvpn-install) - script to set up OpenVPN server and configure client
* [Nyr/wireguard-install](https://github.com/Nyr/wireguard-install) - script to set up Wireguard server and configure client
* https://stellarium.org/
* [Browsh](https://www.brow.sh/) - Firefox based console browser with media and scripts support
* [carbonyl](https://github.com/fathyb/carbonyl) - Chromium based console browser with media and scripts support
* [single-file-cli](https://github.com/gildas-lormeau/single-file-cli) - save html web page with **all** resources as a **one** single html file (script, images etc)
  * Uses docker with Puppeteer or Selenium WebDriver inside
* [Easy Effects](https://github.com/wwmm/easyeffects) (PulseEffects) - audio equalizer, there is **old** version in Ubuntu repository
  * **Note.** On laptop it is useful to set up higher Bass Boost to enhance bad audio speakers
* [earlyoom](https://github.com/rfjakob/earlyoom) (The Early OOM Daemon) - **Optionally!**, auto kill any process that hang Ubuntu by consuming to much RAM.
  * **Note.** In new Ubuntu there is **no reason** to use it.
* [unblob](https://unblob.org/installation/) - unpack a lot of unknown archive formats
  ```sh
  # varian 1
  python3 -m pip install --user unblob

  sudo apt install android-sdk-libsparse-utils e2fsprogs p7zip-full unar zlib1g-dev liblzo2-dev lzop lziprecover libhyperscan-dev zstd

  curl -L -o sasquatch_1.0_amd64.deb https://github.com/onekey-sec/sasquatch/releases/download/sasquatch-v4.5.1-4/sasquatch_1.0_amd64.deb
  sudo dpkg -i sasquatch_1.0_amd64.deb
  rm sasquatch_1.0_amd64.deb

  unblob --show-external-dependencies

  # variant 2, docker
  docker run --rm --pull always -v /path/to/extract-dir/on/host:/data/output -v /path/to/files/on/host:/data/input ghcr.io/onekey-sec/unblob:latest /data/input/path/to/file
  docker run --rm --pull always ghcr.io/onekey-sec/unblob:latest --help
  ```
* [mpv](https://www.smplayer.info/downloads) (smplayer) - use built in **mpv** from cache of the application
  * E.g. `/tmp/.mount_SMPlaywRXSsL/usr/bin/mpv`
* [mplayer](https://www.smplayer.info/downloads) (smplayer) - use built in **mplayer** from cache of the application
  * E.g. `/tmp/.mount_SMPlaywRXSsL/usr/bin/mplayer`
* [ffmpeg](https://kdenlive.org/en/download/) (kdenlive) - use built in **ffmpeg** from cache of the application
  * E.g. `/tmp/.mount_kdenliyYVkv3/usr/bin/`
* [PyGlossary](https://github.com/ilius/pyglossary) - scripts to converts various dictionary formats

## KDE
```sh
sudo apt install krename kompare kdiff3 arj unace-nonfree rar unrar kget lhasa digikam kwave kid3 kamoso dolphin-plugins kget krusader k3b kgpg kcharselect kalzium krita kdenlive usb-creator-kde kruler knotes kalgebra kaddressbook kubuntu-restricted-extras kwin-bismuth latte-dock adapta-kde ktimer kronometer umbrello kchmviewer kmail kolourpaint
```

## Official KDE PPAs only
```sh
# Optional! Not recommended! Kubuntu PPA with fresh KDE
sudo add-apt-repository ppa:kubuntu-ppa/backports

# krita
sudo add-apt-repository ppa:kritalime/ppa

# kdenlive
sudo add-apt-repository ppa:kdenlive/kdenlive-stable
```

## Essential KDE setting configuration

* Disable recent files, recent contacts, recent applications in Application Menu
  ```
  > Application Menu
    > right click on recent files, recent contacts, recent applications
      > clear history

  > Application Menu
    > Configure Application Menu
      > disable recent files, recent contacts, recent applications
  ```

# Alternative Kernel with configuration and patches
* [XanMod Kernel](https://xanmod.org/) - optimized for desktop PC

# OS disk image like applications
* [GNOME Partition Editor (GParted)](https://gparted.org)
* [Rescuezilla](https://rescuezilla.com/)
* [Kali Linux](https://www.kali.org/)
* [SystemRescue](https://www.system-rescue.org/)

# Development

## Soft and repositories
```sh
sudo apt install sqlitebrowser wxhexeditor clonezilla git-cola meld wireshark

# KDE
sudo apt install okteta

# git
sudo add-apt-repository ppa:git-core/ppa
sudo apt update
sudo apt install git git-gui git-extras gist

# virtualbox

## https://www.virtualbox.org/wiki/Downloads
## Install Oracle VM VirtualBox Extension Pack by the link above!!!

wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.virtualbox.org/virtualbox/debian $(lsb_release -cs) contrib"
sudo apt-get update
sudo apt-get install virtualbox-6.1
# add user to vboxusers group if needed
sudo groupadd vboxusers
sudo usermod -aG vboxusers $USER
newgrp vboxusers # you may need reboot here

# docker
sudo apt update
sudo apt install ca-certificates curl gnupg lsb-release
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt update
sudo apt install docker-ce docker-ce-cli containerd.io docker-compose-plugin
# fix docker after installation (create group and add user)
sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker # you may need reboot here
```

## Custom repository or no repository
* [vscode](https://code.visualstudio.com/)
* [SDK Platform-Tools](https://developer.android.com/studio/releases/platform-tools) - adb, fastboot, systrace
* [nvm](https://github.com/nvm-sh/nvm)
* [dbeaver](https://dbeaver.io/)
* nmap rpm https://nmap.org/download.html
* [wireshark](https://www.wireshark.org)
* [SmartGit](https://www.syntevo.com/smartgit/)
* [privacy.sexy](https://github.com/undergroundwires/privacy.sexy)

# Recipes

## How to fix broken packages?

```sh
# fix missing
sudo apt --fix-missing update

# force the installation of the broken packages
sudo apt install -f

# remove broken
sudo dpkg --remove --force-remove-reinstreq

# print list of all the packages marked as Required by dpkg
sudo dpkg -l | grep ^..r

# force configure when configure was skipped
sudo dpkg --configure -a

# force remove broken
sudo dpkg --remove --force-remove-reinstreq

# remove the lock file manually
sudo rm /var/lib/apt/lists/lock

# remove the lock in cache
sudo rm /var/cache/apt/archives/lock

# clean up the package cache
sudo apt clean
```

## What is HWE (Hardware Enablement Stacks)? How to upgrade kernel and other to current version?
[HWE (Hardware Enablement Stacks)](https://wiki.ubuntu.com/Kernel/LTSEnablementStack) - ubuntu has some kernel version, after Ubuntu upgraded kernel version stay the same because of lts. To get a new kernel version when you update Ubuntu you need to install **hwe** package.

If you use Ubuntu `xx.xx.2` and newver then you have **hwe** installed by default and you will get **new** kernel version.

There are another **HWE** packages not only for kernel! Search by `hwe` name in your package manager.

**WARNING!** If some drivers does not work after HWE upgrade then reinstall theme.

```sh
# For Ubuntu 22.04!
# If you have another Ubuntu version then check an instruction in internet.

# enable kernel auto upgrade (disabled by default)
sudo apt install --install-recommends linux-generic-hwe-22.04

# disable auto kernel upgrade
# remove linux-generic-hwe-20.04
sudo apt remove linux-{image,headers}-generic-hwe-22.04
sudo apt install linux-generic # install lts kernel

# print all kernels and its dependencies
# remove all kernels except version that you needed
apt list --installed | grep -Eo "^linux-(image|headers|modules|hwe|objects|signatures).*" | awk -F '/' '{print $1}'
```

## Unable to get list of updates: Failed to update metadata for lvfs

Run to solve `Unable to get list of updates: Failed to update metadata for lvfs`, see [Unable to get list of updates: Failed to update metadata for lvfs](https://gitlab.gnome.org/GNOME/gnome-software/-/issues/1127). It is not only for KDE or GNOME, affects many linux systems.
```sh
fwupdmgr --force refresh
```

## Fix for a bug when WSLView try to open any file instead of be opened by target application
```sh
sudo apt purge wslu
```

## Install drivers
```sh
sudo ubuntu-drivers autoinstall
```

## Install discrete nvidia video card driver and run an application
Print list of devices and drivers
```sh
ubuntu-drivers devices
```
Install a nvidia driver (pick a nvidia driver name from the list above)
```sh
sudo apt install nvidia-driver-470
```
Print list of applications run on nvidia card (`nvidia-smi` will be installed with a `nvidia-driver-*` package)
```sh
nvidia-smi
```

Run an application in with nvidia card
```sh
# print video card name
__NV_PRIME_RENDER_OFFLOAD=1 __GLX_VENDOR_LIBRARY_NAME=nvidia glxinfo | grep "OpenGL renderer"

# for example you can run steam
__NV_PRIME_RENDER_OFFLOAD=1 __GLX_VENDOR_LIBRARY_NAME=nvidia steam

# Paste in property of a Steam game 'parameters' to launch on nvidia card
__NV_PRIME_RENDER_OFFLOAD=1 __GLX_VENDOR_LIBRARY_NAME=nvidia %command%
```

## Disable and uninstall `apport`
```sh
sudo systemctl stop apport.service
sudo systemctl disable apport.service
sudo service apport stop
sudo apt purge apport
sudo apt purge whoopsie
```

## Possible migration from `libinput` touchpad driver to `synaptics` driver
Why? Some application to configure touchpad may need `synaptics` to work. For example `KDE` touchpad configuration show much more options if you install `synaptics`. Also you may try `synaptics` if you have problems with touchpad.

```sh
# check driver (should print libinput)
grep -i "Using input driver" /var/log/Xorg.0.log

# install synaptics
sudo apt install xserver-xorg-input-synaptics

# Rollback. If you need to go back to libinput just remove synaptics
sudo apt remove xserver-xorg-input-synaptics
```

## Right way to remove PPA repository
After that Ubuntu will install stable versions of removed packages from Official Ubuntu repository instead of these removed packages from PPA.
```sh
# remove PPA
sudo ppa-purge ppa:whatever/ppa

# check the PPA keys that may stay in your system
apt-key list

# E.g. remove a useless key
sudo apt-key del 790B C727 7767 219C 42C8  6F93 3B4F E6AC C0B2 1F32
```

## How to run bash script as sudo on Ubuntu start up?
You need to make **systemd** service and set up a `script.sh` with sudo commands.

**Step 1.** Make **systemd** `/home/user/start.service` file
```systemd
[Unit]
Description=Run on start

[Service]
ExecStart=/home/user/start.sh

[Install]
WantedBy=multi-user.target
```
**Step 2.** Make sh `/home/user/start.sh` file
```sh
sudo echo "Hello!"
```
**Step 3.** Install and start **systemd** service
```sh
# enable
sudo systemctl enable /home/user/start.service

# start
sudo systemctl start start.service

# check
systemctl status start.service

# stop if needed
systemctl stop start.service

# disable if needed
systemctl disable start.service
```

## Fix `/home` directory file rights
Gives `read` rights for current user, forbides `execute` rights for all. Directory has to has `execute` right to be opened.
```sh
sudo chown -R $USER:$(id -gn $USER) /home
sudo chmod -R ugo=rwx,ugo-x,go-w,ugo+X /home
```

## Disable KDE available updates notification
```sh
sudo rm /etc/xdg/autostart/org.kde.discover.notifier.desktop
```

## Optional. Remove snap
```sh
snap list
snap remove bla1 bla2 # ..., remove all

sudo systemctl disable snapd.service
sudo systemctl disable snapd.socket
sudo systemctl disable snapd.seeded.service

sudo rm -rf /var/cache/snapd/

sudo apt autoremove --purge snapd
```

## Touchpad control
**Why?** Some linux distributes (like Kubuntu 22.04) do not have some functions. **E.g.** a function to switch off **Touchpad** on a **USB Mouse** connected.

1. For non Gnome distros. **Disable Touchpad on USB Mouse On** script.
    ```sh
    sudo curl -L https://gitlab.com/blog.awesomesoft/blog.awesomesoft/-/raw/master/src/linux/sh/off_touchpad_on_usb_mouse.sh -o ~/off_touchpad_on_usb_mouse.sh
    sudo chmod a+rx ~/off_touchpad_on_usb_mouse.sh
    ```
2. For Gnome distros. Set option to **Disable Touchpad on USB Mouse On**.
    ```sh
    gsettings set org.gnome.desktop.peripherals.touchpad send-events disabled-on-external-mouse
    ```
3. [Touchpad-Indicator](https://github.com/atareao/Touchpad-Indicator) - third party utility to control touchpad

# Useful commands
## KDE
```sh
# Source
# https://userbase.kde.org/KDE_Connect/Tutorials/Useful_commands

# Restart KDE
kquitapp5 plasmashell || killall plasmashell && kstart5 plasmashell

# reset display settings
sudo rm -rf ~/.local/share/kscreen

# Brightness settings
qdbus org.kde.Solid.PowerManagement /org/kde/Solid/PowerManagement/Actions/BrightnessControl org.kde.Solid.PowerManagement.Actions.BrightnessControl.setBrightness $(expr $(qdbus org.kde.Solid.PowerManagement /org/kde/Solid/PowerManagement/Actions/BrightnessControl org.kde.Solid.PowerManagement.Actions.BrightnessControl.brightness) + 375)

# Turn off screen
sleep 0.1 && qdbus org.kde.kglobalaccel /component/org_kde_powerdevil invokeShortcut "Turn Off Screen"

# Volume down or up
qdbus org.kde.kglobalaccel /component/kmix invokeShortcut "decrease_volume"
qdbus org.kde.kglobalaccel /component/kmix invokeShortcut "increase_volume"

# Mute
qdbus org.kde.kglobalaccel /component/kmix invokeShortcut "mute"

# Mute microphone
qdbus org.kde.kglobalaccel /component/kmix invokeShortcut "mic_mute"
```

## Device control
```sh
cat /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor # check cpu mode

# set cpu mode
#
# schedutil mode is default for ubuntu 22.04
# https://wiki.archlinux.org/title/CPU_frequency_scaling#Scaling_governors
# https://wiki.debian.org/CpuFrequencyScaling
# https://askubuntu.com/questions/1021748/set-cpu-governor-to-performance-in-18-04
#
# performance 	Run the CPU at the maximum frequency.
# powersave 	Run the CPU at the minimum frequency.
# userspace 	Run the CPU at user specified frequencies.
# ondemand 	Scales the frequency dynamically according to current load. Jumps to the highest frequency and then possibly back off as the idle time increases.
# conservative 	Scales the frequency dynamically according to current load. Scales the frequency more gradually than ondemand.
# schedutil 	Scheduler-driven CPU frequency selection (CPU сам выбирает режим) 
echo powersave | sudo tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor
echo performance | sudo tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor
echo userspace | sudo tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor
echo ondemand | sudo tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor
echo conservative | sudo tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor
echo schedutil | sudo tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor

cat /sys/devices/system/cpu/cpufreq/boost # check cpu turbo
echo "0" | sudo tee /sys/devices/system/cpu/cpufreq/boost # off cpu turbo
echo "1" | sudo tee /sys/devices/system/cpu/cpufreq/boost # on cpu turbo

# UVC usually is a standard laptop camera
# https://en.wikipedia.org/wiki/USB_video_device_class
sudo modprobe -r uvcvideo # disable laptop UVC camera
sudo modprobe -a uvcvideo # enable laptop UVC camera

sudo dmidecode bios-version # info about BIOS
```