Source: [1](https://www.kernel.org/doc/html/latest/block/bfq-iosched.html), [2](https://unix.stackexchange.com/a/376136)

It is instruction about how to enable BFQ scheduler.  
**Why?** Because default scheduler cause lags when you work with slow devices (e.g. work with slow USB Flash Drive and a lot of small files)

Execute commands below step by step:
```bash
# check default scheduler
cat /sys/block/sda/queue/scheduler      # hdd SATA
cat /sys/block/nvme0n1/queue/scheduler  # ssd NVMe

# enable bfq module
sudo modprobe bfq
sudo echo "bfq" >> /etc/modules-load.d/bfq.conf
sudo echo "ACTION==\"add|change\", KERNEL==\"sd[a-z]|vd[a-z]|sr[0-9]*|mmcblk[0-9]*|nvme[0-9]*n[0-9]*\", ATTR{queue/scheduler}=\"bfq\"" >> /etc/udev/rules.d/60-scheduler.rules
# may need reboot on the step
sudo udevadm control --reload
sudo udevadm trigger

# enable in grub
kate /etc/default/grub
# add "scsi_mod.use_blk_mq=1" to GRUB_CMDLINE_LINUX, it will looks like: GRUB_CMDLINE_LINUX="quiet scsi_mod.use_blk_mq=1"
sudo update-grub
# reboot on the step

# test
cat /sys/block/sda/queue/scheduler      # hdd SATA
cat /sys/block/nvme0n1/queue/scheduler  # ssd NVMe
```