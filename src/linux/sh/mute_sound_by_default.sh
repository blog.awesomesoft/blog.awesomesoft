#!/bin/bash

# The script will auto mute all new opened applications, they can by unmute from system sound menu
pactl subscribe | stdbuf -o0  grep "sink" | stdbuf -o0 awk '{print $2, $5}' | stdbuf -o0 grep -P "^'new'\s" | stdbuf -o0 awk '{print $2}' | stdbuf -o0 grep -Eo '[0-9]{1,}' | while IFS= read -r line;
do 
	notify-send "Application was muted. Sink number: $line"
	pactl set-sink-input-mute $line toggle
done