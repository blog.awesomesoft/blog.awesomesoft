#!/bin/bash

# The script listen mouse plug in event. And when mouse is pluged in then touchpad will be off.
# The script works with one touchpad only!
# The script works with USB mouse only (or mouse that emulate USB connection)!
# The script works with any quantity of usb mice.

TID=`xinput list | grep -Eo '\sTouchpad\s+id\=[0-9]{1,}' | grep -Eo '[0-9]{1,}'`
MCOUNT=`xinput list | grep -Eo '\sMouse\s+id\=[0-9]{1,}' | grep -c ^`

# Fake touchpad count
# Add any fake touchpad to the expression, you can find it with "xinput list" command
#
# e.g. ELAN is an ASUS touchpad that have "Mouse" word in its name
# https://github.com/mishurov/linux_elan1200_touchpad
FAKE_MCOUNT=`xinput list | grep -Eo 'ELAN.+\sMouse\s+id\=[0-9]{1,}' | grep -c ^`

MCOUNT=$((MCOUNT-FAKE_MCOUNT))

function inc_mouse_count {
	MCOUNT=$((MCOUNT+1))
}
function dec_mouse_count {
	MCOUNT=$((MCOUNT-1))
}
function touchpad_off {
	if [ $MCOUNT -eq 1 ]
	then
		xinput --disable $TID
		notify-send "Touchpad is OFF."
	fi
}
function touchpad_on {
	if [ $MCOUNT -lt 1 ]
	then
		xinput --enable $TID
		notify-send "Touchpad is ON."
	fi
}

if [ $MCOUNT -gt 0 ]
then
	touchpad_off
else
	touchpad_on
fi

# RegExp explanation https://www.debuggex.com/r/4aJKR6XKjYFO1-1o
udevadm monitor -k | stdbuf -o0 grep -P '^KERNEL\[' | stdbuf -o0 awk '{print $2, $3, $4}' | stdbuf -o0 grep -P '^(add|remove)\s/devices/[^/\s]+/[^/\s]+/[^/\s]+/usb\d+[^\s]+/mouse\d+\s\(input\)$' | stdbuf -o0 awk '{print $1}' | while IFS= read -r line;
do
	if [ "$line" = "add" ]
	then
		inc_mouse_count
		touchpad_off
	elif [ "$line" = "remove" ]
	then
		dec_mouse_count
		touchpad_on
	fi
done
