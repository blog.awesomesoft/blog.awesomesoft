#!/bin/bash

## Source: https://askubuntu.com/a/1021033

amixer -D pulse set Master 0% mute;
acpi_listen | while IFS= read -r line;
do
    if [ "$line" = "jack/headphone HEADPHONE plug" ]
    then
       amixer -D pulse set Master 0% mute
       notify-send "headphones connected. Sound is muted."
    elif [ "$line" = "jack/headphone HEADPHONE unplug" ]
    then
       amixer -D pulse set Master 0% mute
       notify-send "headphones disconnected. Sound is muted."
    fi
done
