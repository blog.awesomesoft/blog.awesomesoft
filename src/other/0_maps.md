Карты навигации, статистики

* **Google**
  * [Maps](https://www.google.ru/maps/)
    * **Слои** - дополнительная информация на карта которую можно включать
      * **Street View** - фото местности которые пользователи загружают сами через приложение **Google Street View**
      * **Traffic** - дорожный трафик
      * **Wildfires** - лесных пожаров
  * [Google Earth](https://earth.google.com/web/) - карта планеты Земля, качественные спутниковые снимки, просмотр оцифрованных улиц в 3D, разные фильтры и инструменты и прочее
    * **Note.** На момент написания в веб версии не работают **Google Earth Mars**, **Google Earth Moon**, **Google Earth Sky**, работают только в **Google Earth Pro**
  * [Google Earth Pro](https://www.google.com/earth/about/versions/) - оффлайн с загрузкой данных в кэш по Интернету, расширенная версия **Google Earth** в виде программы. 
    * **Дополнительно содержит карты:** Марса, Луны, снимки звездного неба (звезды, туманности, события на небе)
  * [Google Mars](https://www.google.com/mars/) - снимки Марса в разных спектрах
* **Yandex**
  * [Maps](https://maps.yandex.com/)
  * [Yandex.Weather](https://yandex.com/weather/) - погода, световой день, влажность, облачность, карта осадков м молний с возможностью проматывать прогноз по времени, карта ветров, карта температур etc
    * [Yandex.Nowcast](https://yandex.com/weather/maps/nowcast) - карта осадков
    * [Yandex.Temperature](https://yandex.com/weather/maps/temperature) - карта температур
    * [Yandex.Wind](https://yandex.com/weather/maps/wind) - карта ветров
    * [Yandex.Pressure](https://yandex.com/weather/maps/pressure) - карта давления
* **OpenStreetMap**
  * [OpenStreetMap](https://www.openstreetmap.org) - относительно свободные бесплатные карта
* **Карты радиационного загрязнения**
  * [NUKEMAP MISSILEMAP](http://www.nuclearsecrecy.com/nukemap/) - расчет зоны загрязнения при ядерном ударе
  * [Карта ядерных испытаний в Европе](https://www.nature.com/articles/s41598-020-68736-2/figures/3)
* [flightradar24](https://www.flightradar24.com) - карта маршрутов самолетов в реальном времени
* [stellarium](https://stellarium-web.org/) - карта звездного неба
* [lightningmaps.org](https://www.lightningmaps.org) - карта молний в реальном времени
* [EOSDIS Worldview](https://worldview.earthdata.nasa.gov/) - карты NASA о Земле, пожары, пылевые бури, ураганы etc, сохраняет историю наблюдения и фильтрует по датам
* [Wikimedia maps](https://maps.wikimedia.org/) - карта Земли от wikipedia
* [ventusky.com](https://www.ventusky.com/) - карта погоды, ветра, осадков, снега, молний, волн etc в реальном времени
* [https://sat24.com](https://sat24.com) - карта погоды, ветра, осадков, etc в реальном времени, можно по времени посмотреть прогноз визуально на карте
* [Научный центр оперативного мониторинга Земли](http://ntsomz.ru/) - разные снимки и анализ со спутников Земли
* [Celestia](https://celestia.space) - оффлайн, карта звездного неба и космоса, в реальном времени показ 3D моделей звезд, планет, спутников etc. Можно проматывать время в обе стороны или ускорять.