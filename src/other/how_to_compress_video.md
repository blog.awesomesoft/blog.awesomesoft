Typically phones record video in x264.  
If you use 60fps then size of movies is huge.

```sh
# libx265 for CPU encoding
# hevc_nvenc for GPU encoding

# strong compression, x264 -> x265, reduces fps to 30fps regardless if frame is important or not
ffmpeg -i <input> -filter:v fps=30 -c:v libx265 -vtag hvc1 -c:a copy <output> 

# the same as above but with no frame fps reducing
ffmpeg -i <input> -filter:v fps=30 -c:v libx265 -vtag hvc1 -c:a copy <output>
```