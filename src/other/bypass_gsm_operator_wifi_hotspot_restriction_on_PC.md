Обход ограничения операторами сотовой связи на раздачу wi-fi через телефон через изменение TTL.  

:warning: **ВНИМАНИЕ! ПРОЧИТАЙТЕ ТО ЧТО НАПИСАНО НИЖЕ В БЛОКЕ ЧТОБЫ С ВАС НЕ СНЯЛИ ДЕНЬГИ!**:

| :exclamation:  "Некоторые" операторы сотовой связи после определения того что включена раздача по Wi-Fi АВТОМАТИЧЕСКИ переключаются на дорогой вариант тарифного плана и берут деньги за раздачу по Wi-Fi. Проверьте это в описании вашего тарифного плана. Выбирайте только тех операторов которые не позволяют раздавать по Wi-Fi без подключения дополнительной услуги, в этом случае они просто БЛОКИРУЮТ весь трафик, который вы пытаетесь раздать и Интернет доступен только с телефона и НЕ СНИМАЮТ ДЕНЬГИ. Авто переключение на дорогой тарифный план при раздаче Wi-Fi опасно еще и тем, что оператор не может точно определить раздаете вы по Wi-Fi или нет и ДАЖЕ ЕСЛИ ВЫ НЕ РАЗДАЕТЕ по Wi-Fi такой оператор может ошибочно начать брать с вас деньги! |
|-----------------------------------------|

**Как работает ограничение:** с каждым запросом в Интернет уходит счетчик TTL показывающий через сколько устройств прошел запроса. Если это число меньше чем нужно на 1, то после ухода с телефона запрос уже прошел кого-то и оператор блокирует такой запрос.

**Как работает "умное" ограничение:** Некоторые операторы могут отслеживать специфичные соединения. Например если **Windows** получает обновления по определенным адресам, то много подключений к таким адресам означает что вы раздаете трафик. Этот пример показывает сам принцип, т.к. существуют **Windows Phone** подключающийся к тем же адресам и блокировать за **Windows** никто не будет.

**Как работает "умное" ограничение блокировки торрент:** Некоторые операторы блокируют скачивание по торрент. Тут может быть много способов блокировки. Может отслеживаться большое количество соединений и если их слишком много, то будет блокирование. Может отслеживаться подключение к **торрент трекерам** (сайтам для первоначального обмена списком пиров). Может анализироваться протокол подключения, то есть каждое соединение будет проверено на наличие байт характерных только для торрент протокола и все такие соединения будут отклонены.

**Как обойти стандартную блокировку раздачи по Wi-Fi:** увеличить на 1 счетчик **TTL** на компьютере, чтобы после ухода с телефона счетчик был на 1 больше. Настройки сделанные через команды ниже сбросятся после перезагрузки, их можно добавить в авто загрузку.

**Note!** После выполнения команд ниже может понадобится отключить/включить адаптер на компьютере в настройках сети или включить/отключить режим полета на Android или ноутбуке!

**Для Windows.**  
Выполнить команды и после этого подключиться к Интернет через раздачу wi-fi на телефоне:
```cmd
netsh int ipv4 set glob defaultcurhoplimit=65
netsh int ipv6 set glob defaultcurhoplimit=65
```
Вернуть значения назад когда wi-fi уже не нужен:
```cmd
netsh int ipv4 set glob defaultcurhoplimit=64
netsh int ipv6 set glob defaultcurhoplimit=64
```

**Для Linux.**  
Выполнить команды:
```bash
sudo sysctl -w net.ipv4.ip_default_ttl=65
sudo sysctl -w net.ipv6.ip_default_ttl=65
```
Вернуть значения назад когда wi-fi уже не нужен:
```bash
sudo sysctl -w net.ipv4.ip_default_ttl=64
sudo sysctl -w net.ipv6.ip_default_ttl=64
```

**Для Android.**  
На стандартных телефонах Android изменить эту опцию можно только через **root** доступ. Некоторые производители разблокируют такую опцию и менять ее можно без **root**.
```bash
cat /proc/sys/net/ipv4/ip_default_ttl   # проверяем доступно ли изменение для ipv4
cat /proc/sys/net/ipv6/ip_default_ttl   # проверяем доступно ли изменение для ipv6
```
**Note.** TTL может лежать в файлах `/proc/sys/net/ipv4/ip_default_ttl` и `/proc/sys/net/ipv6/ip_default_ttl`, они доступны только на телефонах с **root**  
**Note.** Для Android есть приложения работающие через **root** как [TTL Master](https://play.google.com/store/apps/details?id=ru.glebchajah.ttlmaster) которые меняет **TTL** через интерфейс.

**Еще варианты:**
1. Можно использовать **129** вместо **65** и **128** вместо **64**. На некоторых системах значение по умолчанию может быть **128** и следовательно **129=128+1**. Это может быть актуально для **ipv6**.
2. Если не сработает можно в настройках сети можно попробовать **отключить** поддержку **ipv6**.

**Как обойти если оператор блокирует раздачу если есть подключение к специфичным адресам Windows. Это очень редкий случай, если вообще существующий, обычно за такое ничего не блокируют.**  
Отключить обновление Windows и различные отправки статистики в Интернет, операторы могут отслеживать такие соединения и понимать что запросы отправлены не с телефона. Например отключить большинство таких запросов можно через [O&O ShutUp10](https://www.oo-software.com/shutup10) (и аналоги). Как вариант нужно использовать VPN.

# Как обойти блокировку скачивания торрент

1. Поставьте в настройках торрент клиента порт **80**, отключите **UPnP/NAT-PMP**
2. включите **Принудительное шифрование**
3. поставьте максимальное количество соединений в **4** или **8** или **16** (примерно, низкое количество может **очень** замедлить начало загрузки)
4. включить **анонимный** режим.
5. Используйте альтернативный DNS сервис с шифрованием, например [DNSCrypt](https://gitlab.com/blog.awesomesoft/blog.awesomesoft/-/blob/master/src/linux/dnscrypt_setup.md).
6. Протокол соединений **TCP & uTP** поменять на **TCP**

Все это может не сработать. Что поможет точно это **proxy** или **VPN**. Из **бесплатных** вариантов можно запустить **Tor Browser** и указать в настройках торрент клиента **SOCKS5** прокси с адресом **127.0.0.1** и портом **9150**, но скорость будет медленной и вы должны понимать чем рискуете в некоторых странах где **Tor** вне закона. При использовании **proxy** или **VPN** в настройках торрент клиента включите опцию **использовать прокси для peer**.

Если вы настраиваете VPN сервер сами на купленном VPS. Некоторые сервисы так же как некоторые провайдеры фильтруют torrent трафик, поэтому может не сработать. Если VPS принадлежит США, Европе, Канаде, Австралии и пр., то torrent трафик может отслеживаться и вам может прийти штраф или VPS может быть отключен.

Как вариант можно использовать [WebTorrent](https://webtorrent.io/) протокол и клиент, это другой протокол, он построен на [WebRTC](https://developer.mozilla.org/en-US/docs/Web/API/WebRTC_API) и блокировщики трафика не могут отличить его от использования например web мессенджеров.

Как вариант использовать [Tribler](https://www.tribler.org/), это программа использует специальные узлы (nodes) сети **Tor**, которые не часть сети **Tor** и используются со специальными настройками, которые не вредят сети **Tor**. По умолчанию **Tribler** использует 1 node между клиентом и источниками торрента, это не безопасно, и сам проект не гарантирует анонимности, он гарантирует только обход ограничений скорости. Узлы (nodes) программы **Tribler** не часть сети **Tor** и созданы отдельными энтузиастами.