Полезные заметки и команды youtube-dl для скачивания с youtube  
Скачать: https://github.com/ytdl-org/youtube-dl  
Утилита ffmpeg для конвертации входил в программу https://kdenlive.org/en/download/ (сборки из других источников не официальные)  
Карта заблокированных роликов для разных стран https://unblockvideos.com/youtube-video-restriction-checker/#url=Y1e8XHcRLwE

Список форматов
```
youtube-dl -F https://www.youtube.com/user/MaybeMalia/videos
```
Скачать видео и извлечь audio в mp3. Если поставить слишком большое значение --http-chunk-size то сервер может замедлить загрузку
```
youtube-dl -ci -o "%(autonumber)s-%(title)s.%(ext)s" -f "bestaudio/best" -x --audio-format mp3 --audio-quality 0 --http-chunk-size 16 --geo-bypass --geo-bypass-country US https://www.youtube.com/user/MaybeMalia/videos
```
Скачать только audio без video (т.к. youtube для некоторых форматов хранит их отдельно). Но Качество может быть хуже чем скачать видео и выделить оттуда аудио.
```
youtube-dl -ci -o "%(autonumber)s-%(title)s.%(ext)s" -f "bestaudio[ext=m4a]" --http-chunk-size 16 --geo-bypass --geo-bypass-country US https://www.youtube.com/user/MaybeMalia/videos
```
Скачать только audio без video с конвертацией в mp3
```
youtube-dl -ci -o "%(autonumber)s-%(title)s.%(ext)s" -f "bestaudio[ext=m4a]" -x --audio-format mp3 --audio-quality 0 --http-chunk-size 16 --geo-bypass --geo-bypass-country US https://www.youtube.com/user/MaybeMalia/videos
```
Через Tor `--proxy socks5://127.0.0.1:9150`
```
youtube-dl -ci -o "%(autonumber)s-%(title)s.%(ext)s" -f "bestaudio/best" -x --audio-format mp3 --audio-quality 0 --http-chunk-size 16 --geo-bypass --geo-bypass-country US --proxy socks5://127.0.0.1:9150 --force-ipv4 https://www.youtube.com/user/MaybeMalia/videos
```