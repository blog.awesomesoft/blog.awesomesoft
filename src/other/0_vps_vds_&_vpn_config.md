Список:
* https://lowendbox.com
* iphoster
* https://ruvds.com/cheap_vps/
* OVH.ie
* Timeweb
* scaleway
* vultr
* https://www.arubacloud.com/
* https://www.lowendtalk.com/discussion/166452/looking-for-vps-unlimited-traffic-in-low-end-cost-prefer-eu
* https://cloudserver.net/billing/cart.php?a=confproduct&i=0

# Настройка VPN на VPS

* обзор инструментов https://habr.com/ru/post/479146/
* openvpn https://github.com/Nyr/openvpn-install
* wireguard https://github.com/Nyr/wireguard-install
* shadowsocks & v2ray https://habr.com/ru/post/555768/
* FileZilla - может подключаться по порту `22` как к FTP

# Закрыть порты
**Note.** `openvpn-install` & `wireguard-install` scripts add its ports to exclusion of iptables so ufw does not affect **OpenVPN** and **Wireguard**
```sh
apt install ufw
ufw status verbose
ufw default deny incoming
ufw default allow outgoing
ufw allow ssh
ufw app list
ufw enable
ufw status verbose
```

# Проверить весь ли объем диск использован для раздела
Если раздел меньше чем диск, то раздел можно увеличить на лету
```sh
df -h
fdisk -l
```
Увеличить размер
```sh
# variant 1
growpart /dev/xvda 1  # Grows the partition
resize2fs /dev/xvda1  # Grows ext* the filesystem, other filesystems have other commands

# variant 2
parted /dev/sda
# (parted) resizepart 1 100%
# (parted) quit
partprobe /dev/xvda # load changed partition table
resize2fs -p /dev/xvda1 # Grows ext* the filesystem, other filesystems have other commands
```

# Подключится через FileZilla
Указать имя, пароль, ip от ssh, порт поставить от ssh т.е. 22 по умолчанию (для FTP по умолчанию он 21 что не подходит)